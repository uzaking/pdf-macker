import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";

// import component
import PdfMaker from './component/pdf_maker';
import HomeLayout from './component/Layout/HomeLayout';
import HomeLayoutExcell from './component/LayoutExcell/HomeLayoutExcell';
import HomeTemplate from './component/Template/HomeTemplate';
import HomeGroup from './component/Group/HomeGroup';
import HomeImage from './component/Image/HomeImage';
import HomePrint from './component/Print/HomePrint';

require('./bootstrap');

render(
  <BrowserRouter>
    <Routes>
        <Route path="/" element={ <PdfMaker /> }></Route>
        <Route path="/layout" element={ <HomeLayout page="" /> }></Route>
        <Route path="/layout/new" element={ <HomeLayout page="new" /> }></Route>
        <Route path="/layout/edit" element={ <HomeLayout page="edit" /> }></Route>
        <Route path="/layout_excell" element={ <HomeLayoutExcell page="" /> }></Route>
        <Route path="/layout_excell/new" element={ <HomeLayoutExcell page="new" /> }></Route>
        <Route path="/template" element={ <HomeTemplate page="" /> }></Route>
        <Route path="/template/new" element={ <HomeTemplate page="new" /> }></Route>
        <Route path="/template/edit" element={ <HomeTemplate page="edit" /> }></Route>
        <Route path="/group" element={ <HomeGroup page="" /> }></Route>
        <Route path="/group/new" element={ <HomeGroup page="new" /> }></Route>
        <Route path="/group/edit" element={ <HomeGroup page="edit" /> }></Route>
        <Route path="/image" element={ <HomeImage page="" /> }></Route>
        <Route path="/image/new" element={ <HomeImage page="new" /> }></Route>
        <Route path="/image/edit" element={ <HomeImage page="edit" /> }></Route>
        <Route path="/print/new" element={ <HomePrint page="new" /> }></Route>
    </Routes>
  </BrowserRouter>
  , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();

