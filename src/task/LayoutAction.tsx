import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper
import { DataLoadHelper } from '../helper/dataload.helper';
import { LayoutHelper } from '../helper/layout.helper';
import { LayoutEditHelper } from '../helper/layout_edit.helper';
import { DataWellHelper } from '../helper/data_well.helper';
import { SpredsheetHelper } from '../helper/spredsheet.helper';

// import Reducer
import { ServerPropsInterface } from '../reducers/Server';
import { PaperPropsInterface } from '../reducers/Paper';
import { ActiveLayoutPropsInterface } from '../reducers/_Layout/ActiveLayout';

import { ListTextPropsInterface } from '../reducers/_Text/ListText';
import { LayoutImagePropsInterface } from '../reducers/_Image/LayoutImage';
import { ListLayoutPropsInterface, initialState as initialLayout } from '../reducers/_Layout/ListLayout';
import { ListTemplatePropsInterface } from '../reducers/_Template/ListTemplate';
import { EditLayoutPropsInterface } from '../reducers/_Layout/EditLayout';
import { ExcellLayoutPropsInterface } from '../reducers/_Layout/ExcellLayout';

const serverParam   = (state: ServerPropsInterface)         => state.Server;
const paperParam    = (state: PaperPropsInterface)          => state.Paper;
const activeLayout  = (state: ActiveLayoutPropsInterface)   => state.ActiveLayout;
const layoutImage   = (state: LayoutImagePropsInterface)    => state.LayoutImage;
const listText      = (state: ListTextPropsInterface)       => state.ListText;
const listLayout    = (state: ListLayoutPropsInterface)     => state.ListLayout;
const listTemplate  = (state: ListTemplatePropsInterface)   => state.ListTemplate;
const editLayout    = (state: EditLayoutPropsInterface)     => state.EditLayout;
const excellLayout    = (state: ExcellLayoutPropsInterface)     => state.ExcellLayout;

// Root Saga登録配列
export const RootLayoutAction = [
    takeEvery('LayoutAction/initialLoad', initialLoad),
    takeEvery('LayoutAction/changeTemplate', changeTemplate),
    takeEvery('LayoutAction/changeExcellTemplate', changeExcellTemplate),
    takeEvery('LayoutAction/atachCSV', atachCSV),
    takeEvery('LayoutAction/save', Save),
    takeEvery('LayoutAction/edit', Edit),
    takeEvery('LayoutAction/update', Update),
    takeEvery('LayoutAction/del', Del),

];

export function* initialLoad(reload: boolean = false): any
{
    const layout = yield select(listLayout);
    if (layout[0] === initialLayout[0]) {
        // データローダー呼び出し、一発目のコールになるのでサーバー情報を渡す
        yield DataLoadHelper.call(
                Object.assign({},yield select(serverParam))
            ).loadAllLayout();

        yield put({
            type    : 'ListLayout/set',
            layouts : LayoutHelper.call().convLayout(
                        DataLoadHelper.call().getResult()
                    )
        });
    }
}

/**
 * 背景イメージの変更
 * @param val {
 *     template: TemplateInterface
 * }
 */
export function* changeTemplate(val: any): any
{
    console.log('change use Template');

    // 背景テンプレートデータをレイアウト編集用Reducerに渡す
    yield put({
        type        : 'ActiveLayout/setTemplate',
        svg         : val.template.svg,
        id          : val.template.id,
    });

    // 用紙サイズ一覧を取得
    const paper = LayoutHelper.call()
                    .getPaperState(
                        yield select(paperParam),
                        val.template.paper,
                        val.template.orientation
                    );
    
    // 用紙サイズをレイアウト編集用Reducerに渡す
    yield put({
        type        : 'ActiveLayout/setPaper',
        paper       : paper.name,
        ...paper
    });
}

/**
 * 背景イメージの変更(Excell)
 * @param val {
 *     template: TemplateInterface
 * }
 */
export function* changeExcellTemplate(val: any): any
{
    console.log('change use Excell Template');
  
    // 
    const contents = yield SpredsheetHelper
                        .call()
                        .getCellContents(val.template.sheet, '');
     
    // 用紙サイズをレイアウト編集用Reducerに渡す
    yield put({
        type        : 'ExcellLayout/setContents',
        contents    : contents,
    });
    yield put({
        type        : 'ExcellLayout/setSheet',
        sheet       : val.template.sheet,
    });
}

export function* atachCSV(val: any): any
{
    const layout = JSON.parse(JSON.stringify(yield select(excellLayout)));
    yield put({
        type    : 'ExcellLayout/setContents',
        contents: LayoutHelper.call().replaceCSV(layout.contents, val.csv)
    });
}

/**
 * 
 */
export function* Save(): any
{
    console.log('Save New Layout');
    const temp  = yield select(activeLayout);
    const texts = yield select(listText);
    const img   = yield select(layoutImage);

    const layout = Object.assign({}, initialLayout[0]);
    layout.template    = temp.temp_id;
    layout.paper       = temp.paper;
    layout.width       = temp.width;
    layout.height      = temp.height;
    layout.texts       = texts.texts;
    layout.images      = img;
    layout.svg         = temp.svg;

    yield DataLoadHelper.call().saveLayout(layout);

    yield reset();
    yield initialLoad(true);
    yield put({
        type    : 'NewLayout/setDone',
        done    : true
    });
}

/**
 * レイアウト再編集モード開始
 * @param val {
 *  layout  : LayoutInterface,  // 編集対象のレイアウトオブジェクト
 *  key     : number            // ListLayoutのIndex番号
 * }
 */
export function* Edit(val: any): any
{
    console.log('Show Edit Layout');

    const temps = DataWellHelper
                    .call(yield select(listTemplate))
                    .getById(val.layout.template);

    yield put({
        type    : 'ListText/set',
        texts   : val.layout.texts
    });
    yield put({
        type    : 'LayoutAction/changeTemplate',
        template: temps,
    });
    yield put({
        type    : 'LayoutImage/set',
        images   : val.layout.images
    });
    yield put({
        type    : 'EditLayout/set',
        layout  : val.layout,
    });
}

export function* Update(): any
{
    console.log('Update Layout');
    const temp  = yield select(activeLayout);
    const texts = yield select(listText);
    const img   = yield select(layoutImage);

    const layout = Object.assign({}, yield select(editLayout));
    layout.template    = temp.temp_id;
    layout.paper       = temp.paper;
    layout.width       = temp.width;
    layout.height      = temp.height;
    layout.texts       = texts.texts;
    layout.images      = img;
    layout.svg         = temp.svg;

    yield DataLoadHelper.call().updateLayout(layout);

    yield reset();
    yield initialLoad(true);
    yield put({
        type    : 'NewLayout/setDone',
        done    : true
    });
    yield put({
        type    : 'NewLayout/setBack',
        back    : '/layout'
    });
}

export function* Del(val: any): any
{
    const re = yield DataLoadHelper.call().deleteLayout({
        id  : val.id
    });

    if (re) {
        yield put({
            type    : 'ListLayout/del',
            key     : val.key,
        });
    }
    yield initialLoad(true);
}

export function MoveStart(e: any): any
{
    LayoutEditHelper.call().moveOn(e);
}

export function* Move(e: any): any
{
    const leh = LayoutEditHelper.call();
    if (leh.checkFlag('resize')) {
        const w = leh.resizeCalc(e);
        yield put({
            type    : 'LayoutImage/update',
            image    : {
                ...e.editTarget,
                ...{
                    key : e.key,
                    width   : w,
                    height  : w * (e.width / e.height),
                }
            }
        });
    }
    if (leh.checkFlag('text')) {
        yield put({
            type    : 'ListText/update',
            text    : {
                ...e.editTarget,
                ...{
                    key : e.key,
                    x   : e.editTarget['x'] + leh.roundCalc(e.pageX, e.rate, 0),
                    y   : e.editTarget['y'] + leh.roundCalc(e.pageY, e.rate, 1)
                }
            }
        });
    }
    if (leh.checkFlag('image')) {
        yield put({
            type    : 'LayoutImage/update',
            image    : {
                ...e.editTarget,
                ...{
                    key : e.key,
                    x   : e.editTarget['x'] + leh.roundCalc(e.pageX, e.rate, 0),
                    y   : e.editTarget['y'] + leh.roundCalc(e.pageY, e.rate, 1)
                }
            }
        });
    }
    if (leh.checkFlag('screen')) {
        yield put({
            type    : 'ActiveLayout/setXY',
            x       : e.screenX - leh.roundCalc(e.pageX, e.rate, 0),
            y       : e.screenY - leh.roundCalc(e.pageY, e.rate, 1)
        });
    }
}

export function MoveEnd(e: any): any
{
    LayoutEditHelper.call().moveEnd(e);
}



export function* reset(): any
{
    yield put({
        type    : 'ActiveLayout/reset'
    });
    yield put({
        type    : 'ListText/reset'
    });
    yield put({
        type    : 'LayoutImage/reset'
    });
    yield put({
        type    : 'NewLayout/reset'
    });
    yield put({
        type    : 'EditLayout/reset'
    });
}