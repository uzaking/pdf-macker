import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper
import { AjaxHelper } from '../helper/ajax.helper';
import { DataLoadHelper } from '../helper/dataload.helper';
import { FileHelper } from '../helper/file.helper';

// import Reducer
import { ServerPropsInterface } from '../reducers/Server';
import { NewImagePropsInterface } from '../reducers/_Image/NewImage';
import { EditImagePropsInterface } from '../reducers/_Image/EditImage';
import { ListImagePropsInterface, initialState as ListInitial } from '../reducers/_Image/ListImage';

const serverParam = (state: ServerPropsInterface) => state.Server;
const newImage = (state: NewImagePropsInterface) => state.NewImage;
const editImage = (state: EditImagePropsInterface) => state.EditImage;
const listImage = (state: ListImagePropsInterface) => state.ListImage;

// Root Saga登録配列
export const RootImageAction = [
    takeEvery('ImageAction/initialLoad', initialLoad),
    takeEvery('ImageAction/saveNewImage', saveNewImage),

    takeEvery('ImageAction/loadAllImage', loadAllImage),

];

export function* initialLoad(): any
{
    const lt = yield select(listImage);
    if (lt === ListInitial) {
        yield loadAllImage()
    }
}

export function* saveNewImage(val: any): any
{
    console.log('save new template');
    const img = yield select(newImage);
    yield callAPI('new', {name: '', image: img.org_image, memo: ''});
}

export function* loadAllImage(): any
{
    console.log('update client flag');
    yield put({
        type    : 'ListImage/set',
        images  : yield callAPI('load')
    });
}


export function* callAPI(
    job: string, body: any = {}
): any {
    const dl = DataLoadHelper.call(yield select(serverParam));

    if (job === 'load') {
        yield dl.loadAllImage();
        return dl.getResult();
    }
    if (job === 'save') {
        yield dl.saveImage(body);
        yield put({
            type    : 'ImageAction/loadAllImage'
        });
    }
    if (job === 'update') {
        yield dl.updateImage(body);
        yield put({
            type    : 'ImageAction/loadAllImage'
        });
    }
    return true;
}

