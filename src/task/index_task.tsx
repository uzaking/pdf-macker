import { all } from 'redux-saga/effects';

// Load AnimationAction
import { AnimationTask } from '../animation/index.task';

import { RootLayoutAction } from './LayoutAction';
import { RootTemplateAction } from './TemplateAction';
import { RootGroupAction } from './GroupAction';
import { RootTextAction } from './TextAction';
import { RootImageAction } from './ImageAction';
import { RootPrintAction } from './PrintAction';
import { RootDragAction } from './DragAction';

export default function* rootSaga() {
    yield all([
        ...RootLayoutAction,
        ...RootTemplateAction,
        ...RootGroupAction,
        ...RootTextAction,
        ...RootImageAction,
        ...RootPrintAction,
        ...RootDragAction,
        ...AnimationTask,
    ]);
}