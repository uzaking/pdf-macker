import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper
import { AjaxHelper } from '../helper/ajax.helper';

// import Reducer
import { ServerPropsInterface } from '../reducers/Server';
const serverParam = (state: ServerPropsInterface) => state.Server;

// Root Saga登録配列
export const RootFlagAction = [
    // フラグ更新
    takeEvery('FlagAction/updateQuestClient', updateQuestClient),
];

export function* updateQuestClient(): any
{
    console.log('update client flag');
    yield callAPI('client', 'quest');
}

export function* updateQuestManager(): any
{
    console.log('update manager flag');
    yield callAPI('manager', 'quest');
}

export function* updateQuestContractor(): any
{
    console.log('update contractor flag');
    yield callAPI('contractor', 'quest');
}

export function* updateReportClient(): any
{
    console.log('update client flag');
    yield callAPI('client', 'report');
}

export function* updateReportManager(): any
{
    console.log('update manager flag');
    yield callAPI('manager', 'report');
}

export function* updateReportContractor(): any
{
    console.log('update contractor flag');
    yield callAPI('contractor', 'report');
}

export function* callAPI(endpoint: string, job: string): any
{
    const ah = AjaxHelper.call();

    // クエストを送信
    const sp = Object.assign({},yield select(serverParam));
    sp.url = sp.url + 'api/public/index.php/api/' + job + '/flag';
    sp.body = {};
    const result = yield ah.callAPI(sp);
    console.log(result);

    const _endpoint = endpoint.charAt(0).toUpperCase() + endpoint.slice(1).toLowerCase();
    

    yield put({
        type        : 'Quest' + _endpoint + '/SetQuest',
        QuestData   : result.message,
    });
}