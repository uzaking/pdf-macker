import { call, put, select, takeEvery } from 'redux-saga/effects';

// import Helper
import { AjaxHelper } from '../helper/ajax.helper';
import { DataLoadHelper } from '../helper/dataload.helper';
import { FileHelper } from '../helper/file.helper';

// import Reducer
import { ServerPropsInterface } from '../reducers/Server';
import { NewTemplatePropsInterface } from '../reducers/_Template/NewTemplate';
import { EditTemplatePropsInterface } from '../reducers/_Template/EditTemplate';
import { ListTemplatePropsInterface, initialState as templateInitial } from '../reducers/_Template/ListTemplate';

const serverParam = (state: ServerPropsInterface) => state.Server;
const newTemplate = (state: NewTemplatePropsInterface) => state.NewTemplate;
const editTemplate = (state: EditTemplatePropsInterface) => state.EditTemplate;
const listTemplate = (state: ListTemplatePropsInterface) => state.ListTemplate;

// Root Saga登録配列
export const RootTemplateAction = [
    takeEvery('TemplateAction/initialLoad'      , initialLoad),
    takeEvery('TemplateAction/saveNewTemplate'  , saveNewTemplate),
    takeEvery('TemplateAction/updateTemplate'   , updateTemplate),
    takeEvery('TemplateAction/loadAllTemplate'  , loadAllTemplate),
];

/**
 * 初回データ読み込み
 */
export function* initialLoad(): any
{
    const lt = yield select(listTemplate);
    if (lt === templateInitial) {
        yield loadAllTemplate()
    }
}

/**
 * 新規テンプレート保存
 */
export function* saveNewTemplate(): any
{
    console.log('save new template');
    yield callAPI('save', yield select(newTemplate));
    yield put({
        type    : 'NewTemplate/setDone',
        done    : true,
    });
}

/**
 * テンプレート更新
 */
export function* updateTemplate(): any
{
    console.log('update client flag');
    const r: any = yield callAPI('update', yield select(editTemplate));
    
    if (r.result) {
        yield put({
            type    : 'EditTemplate/setEnd',
            editend : true
        });
    }
}

/**
 * 全てのテンプレートを読み込み
 */
export function* loadAllTemplate(): any
{
    console.log('load all template');
    yield put({
        type    : 'ListTemplate/set',
        template: yield callAPI('load')
    });
}

/**
 * サーバーAPIの呼び出し
 * @param job string
 * @param body object {}
 * @returns 
 */
export function* callAPI(
    job: string, body: any = {}
): any {
    const dl = DataLoadHelper.call(yield select(serverParam));

    if (job === 'load') {
        yield dl.loadAllTemplate();
        return dl.getResult();
    }
    if (job === 'save') {
        yield dl.saveTemplate(body);
        yield put({
            type    : 'TemplateAction/loadAllTemplate'
        });
    }
    if (job === 'update') {
        yield dl.updateTemplate(body);
        yield put({
            type    : 'TemplateAction/loadAllTemplate'
        });
        return dl.getResult();
    }
    return true;
}

