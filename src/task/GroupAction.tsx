import { put, select, takeEvery } from 'redux-saga/effects';

// import Helper
import { DataLoadHelper } from '../helper/dataload.helper';

// import Reducer
import { ServerPropsInterface } from '../reducers/Server';
import { NewGroupPropsInterface } from '../reducers/_Group/NewGroup';
import { EditGroupPropsInterface } from '../reducers/_Group/EditGroup';
import { Group1PropsInterface, initialState as G1Initial } from '../reducers/_Group/Group1';
import { Group2PropsInterface, initialState as G2Initial } from '../reducers/_Group/Group2';
import { Group3PropsInterface, initialState as G3Initial } from '../reducers/_Group/Group3';

const serverParam = (state: ServerPropsInterface) => state.Server;
const newGroup = (state: NewGroupPropsInterface) => state.NewGroup;
const editGroup = (state: EditGroupPropsInterface) => state.EditGroup;
const gp1 = (state: Group1PropsInterface) => state.Group1;
const gp2 = (state: Group2PropsInterface) => state.Group2;
const gp3 = (state: Group3PropsInterface) => state.Group3;

// Root Saga登録配列
export const RootGroupAction = [
    takeEvery('GroupAction/initialLoad', initialLoad),
    
    takeEvery('GroupAction/saveNewGroup', saveNewGroup),

    takeEvery('GroupAction/saveEditGroup', saveEditGroup),

    takeEvery('GroupAction/loadGroup1', loadGroup1),
    takeEvery('GroupAction/saveGroup1', saveGroup1),
    takeEvery('GroupAction/updateGroup1', updateGroup1),
    takeEvery('GroupAction/loadGroup2', loadGroup2),
    takeEvery('GroupAction/saveGroup2', saveGroup2),
    takeEvery('GroupAction/loadGroup3', loadGroup3),
    takeEvery('GroupAction/saveGroup3', saveGroup3),

];

export function* initialLoad(): any
{
    const g1 = yield select(gp1);
    if (g1 === G1Initial) {
        yield loadGroup1()
    }
    const g2 = yield select(gp2);
    if (g2 === G2Initial) {
        yield loadGroup2()
    }
    const g3 = yield select(gp3);
    if (g3 === G3Initial) {
        yield loadGroup3()
    }
}

export function* allLoad(): any
{
    yield loadGroup1();
    yield loadGroup2();
    yield loadGroup3();
}

export function* saveNewGroup(): any
{
    console.info('save new group');
    const nt = yield select(newGroup);
    if (nt.target === 1) {
        yield saveGroup1(nt);
    } else if (nt.target === 2) {
        yield saveGroup2(nt);
    } else if (nt.target === 3) {
        yield saveGroup3(nt);
    }
}

export function* saveEditGroup(): any
{
    console.info('save edit group');
    const nt = yield select(editGroup);
    if (nt.target === 1) {
        yield put({
            type    : 'Group1/add',
            nt
        });
    } else if (nt.target === 2) {
        yield put({
            type    : 'Group2/add',
            nt
        });
    } else if (nt.target === 3) {
        yield put({
            type    : 'Group3/add',
            nt
        });
    }
}

export function* loadGroup1(): any
{
    console.info('load group 1');
    yield setReducer(1, yield callAPI(1, 'load'));
}
export function* saveGroup1(val: any): any
{
    console.info('save group 1');
    yield callAPI(1, 'new', val);
}
export function* updateGroup1(val: any): any
{
    console.info('update group 1');
    yield callAPI(1, 'update', val);
}

export function* loadGroup2(): any
{
    console.info('load group 2');
    yield setReducer(2, yield callAPI(2, 'load'));
}

export function* saveGroup2(val: any): any
{
    console.log('save group 2');
    yield callAPI(2, 'save', val);
}

export function* loadGroup3(): any
{
    console.log('load group 3');
    yield setReducer(3, yield callAPI(3, 'load'));
}
export function* saveGroup3(val: any): any
{
    console.log('update client flag');
    yield callAPI(3, 'save', val,);
}

export function* callAPI(
    target: number, job: string, body: object = {}
): any {
    const dl = DataLoadHelper.call(yield select(serverParam));

    if (job === 'load') {
        yield dl.loadAllGroup(target);
        return dl.getResult();
    }
    if (job === 'save') {
        yield dl.saveGroup(target, body);
        yield put({
            type    : 'GroupAction/loadGroup' + target,
        });
    }
    return true;
}

export function* setReducer(target, groups): any
{
    yield put({
        type    : 'Group' + target + '/set',
        groups
    });
}

export function* updateReducer(target, group): any
{
    yield put({
        type    : 'Group' + target + '/up',
        group
    });
}

export function* delReducer(target, group): any
{
    yield put({
        type    : 'Group' + target + '/set',
        group
    });
}
