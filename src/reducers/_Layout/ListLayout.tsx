import { createSlice } from '@reduxjs/toolkit';
import {
    EditLayoutInterface,
    editInitialState,
} from './_layout.interface';


export interface ListLayoutPropsInterface
{
    ListLayout? : EditLayoutInterface[];
    dispatch?   : any;
}

export const initialState: EditLayoutInterface[] = [editInitialState]

const slice = createSlice({
    name: 'ListLayout',
    initialState,
    reducers: {
        set: (state: any, action: any) => {
            return action.layouts;
        },
        del: (state: any, action: any) => {
            delete state[action.key];
            return state;
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
