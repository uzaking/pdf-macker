import { TextInterface, initialState as initialText } from '../_Text/_text.Interface';
import { LayoutImageInterface, layoutInitialState } from '../_Image/_Image.Interface';

/**
 * レイアウトのベース
 * @param template number
 * @param group1 number
 * @param group2 number
 * @param group3 number
 * @param name string
 * @param paper string
 * @param width number
 * @param height number
 * @param texts TextInterface[]
 * @param images LayoutImageInterface[]
 * @param svg string
 * @param sheet string
 * @param memo string
 */
export interface LayoutInterface
{
    template : number,
    group1   : number,
    group2   : number,
    group3   : number,
    name     : string,
    paper    : string,
    width    : number,
    height   : number,
    texts    : TextInterface[],
    images   : LayoutImageInterface[],
    svg      : string,
    sheet    : string,
    memo     : string,
    done     : boolean,
    back     : string,
}

/**
 * レイアウト編集用
 * @param id number
 * @param template number
 * @param group1 number
 * @param group2 number
 * @param group3 number
 * @param name string
 * @param paper string
 * @param width number
 * @param height number
 * @param texts TextInterface[]
 * @param images LayoutImageInterface[]
 * @param svg string
 * @param sheet string
 * @param memo string
 * @param editend boolean
 */
export interface EditLayoutInterface extends LayoutInterface
{
    id                  : number,
}

/**
 * Excellレイアウト用
 * @param id number
 * @param template number
 * @param group1 number
 * @param group2 number
 * @param group3 number
 * @param name string
 * @param paper string
 * @param width number
 * @param height number
 * @param texts TextInterface[]
 * @param images LayoutImageInterface[]
 * @param svg string
 * @param sheet string
 * @param memo string
 * @param editend boolean
 */
 export interface ExcellLayoutInterface extends LayoutInterface
 {
    contents        : object,
 }


/**
 * @param paper string
 * @param width number
 * @param height number
 * @param x number
 * @param y number
 * @param svg string
 * @param temp_id number
 */
export interface ActiveLayoutInterface
{
    paper    : string,
    width    : number,
    height   : number,
    rate     : number,
    x        : number,
    y        : number,
    svg      : string,
    temp_id  : number,
}

/**
 * ベース初期データー
 */
export const initialState: LayoutInterface = {
    template        : 0,
    group1          : 1,
    group2          : 1,
    group3          : 1,
    name            : '',
    paper           : 'A4',
    width           : 210,
    height          : 297,
    texts           : [initialText],
    images          : [layoutInitialState],
    svg             : '',
    sheet           : '',
    memo            : '',
    done            : false,
    back            : '../',
}

/**
 * 編集時初期データ
 */
export const editInitialState: EditLayoutInterface = {
    ...initialState,
    ...{
        id          : 0,
    }
}

/**
 * Excell用初期データ
 */
export const excellInitialState: ExcellLayoutInterface = {
    ...initialState,
    ...{
        contents          : {},
    }
}

export const activeInitialState: ActiveLayoutInterface = {
    paper           : 'A4',
    width           : 210,
    height          : 297,
    rate            : 0.8,
    x               : 0,
    y               : 0,
    svg             : '',
    temp_id         : 0,
}