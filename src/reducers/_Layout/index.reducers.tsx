import NewLayout from './NewLayout';
import EditLayout from './EditLayout';
import ListLayout from './ListLayout';
import ExcellLayout from './ExcellLayout';
import ActiveLayout from './ActiveLayout';

export const LayoutReducer = {
    NewLayout               : NewLayout,
    EditLayout              : EditLayout,
    ListLayout              : ListLayout,
    ExcellLayout            : ExcellLayout,
    ActiveLayout            : ActiveLayout,
}
