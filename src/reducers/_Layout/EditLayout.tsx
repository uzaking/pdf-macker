import { createSlice } from '@reduxjs/toolkit';
import {
    EditLayoutInterface,
    editInitialState,
} from './_layout.interface';
export interface EditLayoutPropsInterface
{
    EditLayout?: EditLayoutInterface,
    dispatch?   : any;
}

export const initialState: EditLayoutInterface = editInitialState

const slice = createSlice({
    name: 'EditLayout',
    initialState,
    reducers: {
        set: (state: any, action: any) => {
            return action.layout;
        },
        setTemplateId: (state: any, action: any) => {
            return Object.assign({}, state,
                { template_id     : action.template_id, }
            )
        },
        setGroup1Id: (state: any, action: any) => {
            return Object.assign({}, state,
                { group1_id     : action.group1_id, }
            )
        },
        setGroup2Id: (state: any, action: any) => {
            return Object.assign({}, state,
                { group2_id     : action.group2_id, }
            )
        },
        setGroup3Id: (state: any, action: any) => {
            return Object.assign({}, state,
                { group3_id     : action.group3_id, }
            )
        },
        setName: (state: any, action: any) => {
            return Object.assign({}, state,
                { name     : action.name, }
            )
        },
        setTexts: (state: any, action: any) => {
            return Object.assign({}, state,
                { texts     : action.texts, }
            )
        },
        addText: (state: any, action: any) => {
            return Object.assign(
                    {},
                    ...state.texts,
                    ...action.texts
                )
        },
        setImages: (state: any, action: any) => {
            return Object.assign({}, state,
                { images     : action.images, }
            )
        },
        addImage: (state: any, action: any) => {
            return Object.assign(
                    {},
                    ...state.imagess,
                    ...action.texts
                )
        },
        setMemo: (state: any, action: any) => {
            return Object.assign({}, state,
                { name     : action.name, }
            )
        },
        setDone: (state: any, action: any) => {
            return Object.assign({}, state,
                { done     : action.done, }
            )
        },
        resetContent: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
