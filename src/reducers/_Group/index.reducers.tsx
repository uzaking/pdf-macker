import NewGroup from './NewGroup';
import EditGroup from './EditGroup';
import Group1 from './Group1';
import Group2 from './Group2';
import Group3 from './Group3';

export const GroupReducer = {
    NewGroup               : NewGroup,
    EditGroup              : EditGroup,
    Group1                 : Group1,
    Group2                 : Group2,
    Group3                 : Group3,
}
