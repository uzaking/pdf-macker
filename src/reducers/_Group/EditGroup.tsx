import { createSlice } from '@reduxjs/toolkit';
import { EditGroupInterface, editInitialState } from './_group.interface';

export interface EditGroupPropsInterface
{
    EditGroup?  : EditGroupInterface,
    dispatch?   : any;
}

export const initialState: EditGroupInterface = editInitialState;

const slice = createSlice({
    name: 'EditGroup',
    initialState,
    reducers: {
        set: (state: any, action: any) => {
            return action.group;
        },
        setTarget: (state: any, action: any) => {
            return Object.assign({}, state,
                { target   : action.target, }
            )
        },
        setName: (state: any, action: any) => {
            return Object.assign({}, state,
                { name     : action.name, }
            )
        },
        setMemo: (state: any, action: any) => {
            return Object.assign({}, state,
                { memo     : action.memo, }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
