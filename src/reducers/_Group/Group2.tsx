import { createSlice } from '@reduxjs/toolkit';
import { EditGroupInterface, listInitialState } from './_group.interface';

export interface Group2PropsInterface
{
    Group2?     : EditGroupInterface[],
    dispatch?   : any;
}

export const initialState: EditGroupInterface[] = listInitialState;


const slice = createSlice({
    name: 'Group2',
    initialState,
    reducers: {
        set: (state: any, action: any) => {
            return action.groups;
        },
        update: (state: any, action: any) => {
            for (const key in state) {
                if (Object.prototype.hasOwnProperty.call(state, key)) {
                    if (state[key]['id'] === action['id']) {
                        state[key] = action;
                        return state;
                    }
                }
            }
        },
        add: (state: any, action: any) => {
            return Object.assign({}, ...state, ...action)
        },
        del: (state: any, action: any) => {
            for (const key in state) {
                if (Object.prototype.hasOwnProperty.call(state, key)) {
                    if (state[key]['id'] === action['id']) {
                        delete state[key];
                        return state;
                    }
                }
            }
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
