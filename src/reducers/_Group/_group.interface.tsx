export interface GroupInterface
{
    name        : string
    memo        : any,
}

export interface NewGroupInterface extends GroupInterface
{
    target      : 1 | 2 | 3
}

export interface EditGroupInterface extends GroupInterface
{
    id          : string
}

export const initialState: NewGroupInterface = {
    target      : 1,
    name        : '',
    memo        : '',
}

export const editInitialState: EditGroupInterface = {
    id          : '',
    name        : '',
    memo        : '',
}

export const listInitialState: EditGroupInterface[] = [editInitialState]