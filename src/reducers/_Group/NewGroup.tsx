import { createSlice } from '@reduxjs/toolkit';
import { NewGroupInterface, initialState as groupInitial } from './_group.interface';

export interface NewGroupPropsInterface
{
    NewGroup?  : NewGroupInterface,
    dispatch?   : any;
}

export const initialState: NewGroupInterface = groupInitial


const slice = createSlice({
    name: 'NewGroup',
    initialState,
    reducers: {
        setTarget: (state: any, action: any) => {
            return Object.assign({}, state,
                { target     : action.target, }
            )
        },
        setName: (state: any, action: any) => {
            return Object.assign({}, state,
                { name     : action.name, }
            )
        },
        setMemo: (state: any, action: any) => {
            return Object.assign({}, state,
                { memo     : action.memo, }
            )
        },
        reset: (state: any, action: any) => {
            return initialState;
        }
    }
});

export default slice.reducer;
