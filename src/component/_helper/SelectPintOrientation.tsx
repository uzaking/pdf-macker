import React from "react";
import { connect } from 'react-redux';

interface PrintOrientation {
    dispatch?   : any,
    next        : string,
}

export class SelectPrintOrientation extends React.Component<PrintOrientation, {}>{
    render() {
        
        return (
            <div>
                <label
                    htmlFor="paper-size"
                    className="custom-select-1c col-sm-2">印刷向き:</label>
                <div className="container-fluid">
                    <div className="row">
                        <div className="form-check form-check-inline col">
                            <input type="radio"
                                className="form-check-input"
                                name="rotate"
                                id="custom-radio-1a"
                                onClick={
                                    () => this.props.dispatch({
                                        type        : this.props.next,
                                        orientation : 'portrait',
                                    })
                                }
                            />
                            <label className="form-check-label" htmlFor="custom-radio-1a">　縦向き</label>
                        </div>
                        <div className="form-check form-check-inline col">
                            <input type="radio"
                                className="form-check-input"
                                name="rotate"
                                id="custom-radio-1b"
                                onClick={
                                    () => this.props.dispatch({
                                        type    : this.props.next,
                                        orientation   : 'landscape',
                                    })
                                }
                            />
                            <label className="form-check-label" htmlFor="custom-radio-1b">　横向き</label>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state: any) => {
    return state;
}
export default connect(
    mapStateToProps
)(SelectPrintOrientation)
