import React from 'react';
import { Provider } from 'react-redux'
import { Link } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.css';
import '../_style/app.scss';

import LoadingAnimation from '../../animation/loading.animation';
import ToastrAnimation from '../../animation/toastr.animation';

// import Components
import ListGroup from './ListGroup';
import NewGroup from './NewGroup';
import EditGroup from './EditGroup';

// import rootReducer from './reducers'
import { createStore } from '../../store/configureStore';


const store = createStore();
interface AppPropsInterface {
  dispatch? : any;
  page      : string
}

export default class HomeTemplate
  extends React.Component <AppPropsInterface, {}>
{
  render() {
    return (
      <Provider store={ store }>
        <nav className="navbar navbar-dark bg-dark">
          <Link to="/group" className="large_link navbar-brand">Group</Link>
          <Link to="/" className="large_link navbar-brand">Home</Link>
        </nav>
        <div className="container-fluid">
            <div className="container">
              { this.checkPage(this.props.page) }
            </div>
        </div>
        <LoadingAnimation />
        <ToastrAnimation />
      </Provider>
    )
  }
  private checkPage(page: string): any
  {
    if (page === 'new') {
      return <NewGroup />;
    }
    if (page === 'edit') {
      return <EditGroup />;
    }
    return <ListGroup />;
  }
}
// render (
//    <App />,
//    document.getElementById('regist')
//);