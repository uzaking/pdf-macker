import React from 'react';
import { connect } from 'react-redux';

import {
    NewGroupPropsInterface
} from '../../reducers/_Group/NewGroup'

export class NewGroup
    extends React.Component<NewGroupPropsInterface, {}>
{
    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-dark">
                    <h6>New Group</h6>
                </nav>

                <div className="form-group row">
                    <label
                        htmlFor="group-name"
                        className="col-sm-2
                        col-form-label">グループ名</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            id="group-name"
                            className="form-control"
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'NewGroup/setName',
                                    name    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <label
                        htmlFor="group-memo"
                        className="col-sm-2
                        col-form-label">メモ</label>
                    <div className="col-sm-10">
                        <textarea
                            id="group-memo"
                            className="form-control"
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'NewGroup/setMemo',
                                    memo    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <label
                        htmlFor="paper-size"
                        className="custom-select-1c col-sm-2">グループ種別:</label>
                        {[1, 2, 3].map((value) => {
                            return <div className="form-check form-check-inline">
                                <input type="radio"
                                    className="form-check-input"
                                    name="paper"
                                    id="custom-radio-1{value}"
                                    onClick={
                                        () => this.props.dispatch({
                                            type    : 'NewGroup/setTarget',
                                            target   : value,
                                        })
                                    }
                                />
                                <label className="form-check-label"
                                    htmlFor="custom-radio-1{value}">  グループ{value}</label>
                            </div>
                        })}
                </div>
                <br></br>

                <div className="form-group row">
                    <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        onClick={
                            () => this.props.dispatch({
                                type            : 'GroupAction/saveNewGroup'
                            })
                        }>
                        登録</button>
                </div>
            </div>
        );
    }

}

export default connect(
    (state: any) => {
        return state;
    }
)(NewGroup)
