import React from 'react';
import { connect } from 'react-redux';

// import reducers
import {
    Group1PropsInterface, initialState
} from '../../reducers/_Group/Group1'

interface GroupPropsInterface extends Group1PropsInterface
{
    next    : string,
    prop    : string,
    initial? : number,
}

const initialGroup = {
    ...initialState,
    ...{
        next    : '',
        prop    : '',
        initial : 1,
    }
}

export class SelectGroup1
    extends React.Component<GroupPropsInterface, {}>
{
    private g1: any;
    componentWillMount(){
        this.props.dispatch({
            type    : 'GroupAction/loadGroup1'
        });
        this.g1 = (this.props.Group1) ?
                        this.props.Group1 : initialGroup;
    }
    render() {
        return (
            <div className="form-group row">
            <label
                htmlFor="paper-size"
                className="custom-select-1c col-sm-2">Group1:</label>
                {
                this.g1.map((value: any, key) => {
                    const ch: boolean = (this.props.initial === value.id) ? true : false;
                    return <div className="form-check form-check-inline" key={key}>
                        <input type="radio"
                            className="form-check-input"
                            name="group1"
                            id={"custom-radio-1" + value.id}
                            defaultChecked={ch}
                            onClick={
                                () => this.props.dispatch({
                                    type                : this.props.next,
                                    [this.props.prop]   : value.id,
                                })
                            }
                        />
                        <label className="form-check-label"
                            htmlFor={"custom-radio-1" + value.id}>  {value.name}</label>
                    </div>
                })
                }
        </div>
        );
    }

}

export default connect(
    (state: any) => {
        return state;
    }
)(SelectGroup1)
