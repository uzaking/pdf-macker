import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
// import reducers
import {
    Group1PropsInterface, initialState
} from '../../reducers/_Group/Group1'

export class ListGroup1
    extends React.Component<Group1PropsInterface, {}>
{
    componentDidMount()
    {
        this.props.dispatch({
            type    : 'GroupAction/loadGroup1'
        });
    }
    render() {
        const g1 = (this.props.Group1) ?
                    this.props.Group1 : initialState;
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>名</th>
                        <th>メモ</th>
                    </tr>
                </thead>
                <tbody>
                    { this.buildList(g1, 1) }
                </tbody>
            </table>
        );
    }

    private buildList(groups: any, target: number): any
    {
        console.log(groups);
        const _lists = Object.keys(groups).map((val, key) => {
            return (
                <tr key={key}>
                    <td>{groups[val]['name']}</td>
                    <td>{groups[val]['memo']}</td>
                    <td>
                        <button
                            type="button"
                            className="btn btn-secondary btn-sm"
                            data-toggle="modal" data-target='#show_qr'
                            onClick={
                                () => {
                                    this.props.dispatch({
                                        type    : 'EditGroup/set',
                                        group   : {...groups[val], ...{target: 1}}
                                    });
                                }
                        }>
                            <Link
                                to="/group/edit"
                                className="large_link button_link"
                                >編集</Link></button>
                        <button
                            type="button"
                            className="btn btn-secondary btn-sm"
                            data-toggle="modal" data-target='#show_qr'
                            onClick={
                                () => {
                                    this.props.dispatch({
                                        type    : 'GroupAction/Del',
                                        target  : target,
                                        id      : groups[val]['id']
                                    });
                                }
                        }>削除</button>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListGroup1)
