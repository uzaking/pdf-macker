import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
// import reducers
import {
    Group2PropsInterface, initialState
} from '../../reducers/_Group/Group2'

export class ListGroup2
    extends React.Component<Group2PropsInterface, {}>
{
    componentDidMount()
    {
        this.props.dispatch({
            type    : 'GroupAction/loadGroup2'
        });
    }
    render() {
        const g2 = (this.props.Group2) ?
                    this.props.Group2 : initialState;
        return (
            <table className="table">
                <thead>
                    <tr>
                        <th>名</th>
                        <th>メモ</th>
                    </tr>
                </thead>
                <tbody>
                    { this.buildList(g2, 1) }
                </tbody>
            </table>
        );
    }

    private buildList(groups: any, target: number): any
    {
        console.log(groups);
        const _lists = Object.keys(groups).map((val, key) => {
            return (
                <tr key={key}>
                    <td>{groups[val]['name']}</td>
                    <td>{groups[val]['memo']}</td>
                    <td>
                        <button
                            type="button"
                            className="btn btn-secondary btn-sm"
                            data-toggle="modal" data-target='#show_qr'
                            onClick={
                                () => {
                                    this.props.dispatch({
                                        type    : 'EditGroup/set',
                                        group   : {...groups[val], ...{target: 2}}
                                    });
                                }
                            }>
                            <Link
                                to="/group/edit"
                                className="large_link button_link"
                                >編集</Link></button>
                        <button
                            type="button"
                            className="btn btn-secondary btn-sm"
                            data-toggle="modal" data-target='#show_qr'
                            onClick={
                                () => {
                                    this.props.dispatch({
                                        type    : 'GroupAction/Del',
                                        target  : target,
                                        id      : groups[val]['id']
                                    });
                                }
                        }>削除</button>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListGroup2)
