import React from 'react';
import { connect } from 'react-redux';

// import reducers
import {
    Group2PropsInterface, initialState
} from '../../reducers/_Group/Group2'

interface GroupPropsInterface extends Group2PropsInterface
{
    next    : string,
    prop    : string,
    initial?: number,
}
export class SelectGroup2
    extends React.Component<GroupPropsInterface, {}>
{
    private g2: any;
    componentWillMount(){
        this.props.dispatch({
            type    : 'GroupAction/loadGroup2'
        });
        this.g2 = (this.props.Group2) ?
                this.props.Group2 : initialState;
    }
    render() {
        return (
            <div className="form-group row">
            <label
                htmlFor="paper-size"
                className="custom-select-1c col-sm-2">Group2:</label>
                {
                this.g2.map((value: any, key) => {
                    const ch: boolean = (this.props.initial === value.id) ? true : false;
                    return <div className="form-check form-check-inline" key={key}>
                        <input type="radio"
                            className="form-check-input"
                            name="group2"
                            id={"custom-radio-1" + value.id}
                            defaultChecked={ch}
                            onClick={
                                () => this.props.dispatch({
                                    type                : this.props.next,
                                    [this.props.prop]   : value.id,
                                })
                            }
                        />
                        <label className="form-check-label"
                            htmlFor={"custom-radio-1" + value.id}>  {value.name}</label>
                    </div>
                })}
        </div>
        );
    }

}

export default connect(
    (state: any) => {
        return state;
    }
)(SelectGroup2)
