import React from 'react';
import { connect } from 'react-redux';

import {
    EditGroupPropsInterface, initialState
} from '../../reducers/_Group/EditGroup'

export class EditGroup
    extends React.Component<EditGroupPropsInterface, {}>
{
    render() {
        const id = (this.props.EditGroup) ?
                    this.props.EditGroup : initialState;
        return (
            <div className="container">
                <nav className="navbar navbar-dark">
                    <h6>Edit Group</h6>
                </nav>

                <div className="form-group row">
                    <label
                        htmlFor="group-name"
                        className="col-sm-2
                        col-form-label">グループ名</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            id="group-name"
                            className="form-control"
                            value={id.name}
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'EditGroup/setName',
                                    name    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <label
                        htmlFor="group-memo"
                        className="col-sm-2
                        col-form-label">メモ</label>
                    <div className="col-sm-10">
                        <textarea
                            id="group-memo"
                            className="form-control"
                            value={id.memo}
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'NewGroup/setMemo',
                                    memo    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <label
                        htmlFor="paper-size"
                        className="custom-select-1c col-sm-2">グループ種別:</label>
                        {[1, 2, 3].map((value) => this.buildCheckList(id, value))}
                </div>
                <br></br>
                <div className="form-group row">
                    <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        onClick={
                            () => this.props.dispatch({
                                type            : 'GroupAction/saveNewGroup'
                            })
                        }>
                        更新</button>
                </div>
            </div>
        );
    }

    private buildCheckList(eg: any, val: number): any
    {
        const ch: boolean = (eg.target === val) ? true : false;

        return (
            <div className="form-check form-check-inline">
                <input
                    type="radio"
                    className="form-check-input"
                    name="paper"
                    id={"custom-radio-" + val}
                    defaultChecked={ ch }
                    onClick={
                        () => this.props.dispatch({
                            type    : 'NewGroup/setTarget',
                            target   : val,
                        })
                    }
                />
                <label className="form-check-label"
                    htmlFor={"custom-radio-1" + val}> {'グループ' + val}
                </label>
            </div>
        );
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(EditGroup)
