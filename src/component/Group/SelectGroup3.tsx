import React from 'react';
import { connect } from 'react-redux';

// import reducers
import {
    Group3PropsInterface, initialState
} from '../../reducers/_Group/Group3'

interface GroupPropsInterface extends Group3PropsInterface
{
    next    : string,
    prop    : string,
    initial? : number,
}

const initialGroup = {
    ...initialState,
    ...{
        next    : '',
        prop    : '',
        initial : 1,
    }
}

export class SelectGroup3
    extends React.Component<GroupPropsInterface, {}>
{
    private g3: any;
    componentWillMount(){
        this.props.dispatch({
            type    : 'GroupAction/loadGroup3'
        });
        this.g3 = (this.props.Group3) ?
                        this.props.Group3 : initialGroup;
    }
    render() {
        return (
            <div className="form-group row">
            <label
                htmlFor="paper-size"
                className="custom-select-1c col-sm-2">Group3:</label>
                {
                this.g3.map((value: any, key) => {
                    const ch: boolean = (this.props.initial === value.id) ? true : false;
                    return <div className="form-check form-check-inline" key={key}>
                        <input type="radio"
                            className="form-check-input"
                            name="group3"
                            id={"custom-radio-1" + value.id}
                            defaultChecked={ch}
                            onClick={
                                () => this.props.dispatch({
                                    type                : this.props.next,
                                    [this.props.prop]   : value.id,
                                })
                            }
                        />
                        <label className="form-check-label"
                            htmlFor={"custom-radio-1" + value.id}>  {value.name}</label>
                    </div>
                })}
        </div>
        );
    }

}

export default connect(
    (state: any) => {
        return state;
    }
)(SelectGroup3)
