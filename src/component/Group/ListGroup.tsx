import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
// import reducers
import ListGroup1 from './ListGroup1';
import ListGroup2 from './ListGroup2';
import ListGroup3 from './ListGroup3';

export class ListGroup
    extends React.Component<{}, {}>
{
    render() {
        return (
            <div className="container">
                <Link to="/group/new" className="large_link">ADD</Link>
                <div className="d-flex">
                    <ListGroup1 />
                </div>
                <div className="d-flex">
                    <ListGroup2 />
                </div>
                <div className="d-flex">
                    <ListGroup3 />
                </div>
            </div>
        );
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListGroup)
