import React from 'react';
import { connect } from 'react-redux';

// import component

// import reducer
import {
    NewImagePropsInterface, initialState
} from '../../reducers/_Image/NewImage'


export class NewImage
    extends React.Component<NewImagePropsInterface, {}>
{
    render() {
        const ni = (this.props.NewImage) ?
                    this.props.NewImage : initialState;
        return (
            <div className="container">
                <nav className="navbar navbar-dark">
                    <h6>New Image</h6>
                </nav>
                <br></br>
                <div className="form-group row">
                    <div
                        id="dragimage" className="dragTest center"
                        onDragOver={(e) => this.onDragStart(e)}
                        onDrop={(e) => this.onDragEnd(e)}
                    >
                        
                        { this.checkImage(ni.image) ? '追加済み' : 'ここに画像をドラッグ' }
                        
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <div className="mx-auto" style={{width: "200px"}}>
                        { this.checkImage(ni.image)
                            ? (<img src={ni.image} alt="画像" width="200px"></img>) : '' }
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    {(ni.image !== '') ? (
                        <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        onClick={
                            () => this.props.dispatch({
                                type    : 'ImageAction/saveNewImage',
                            })
                        }>
                        登録</button>
                    ) : ''}
                </div>
            </div>
        );
    }

    private checkImage(image: string): boolean
    {
        return (image !== '') ? true : false;
    }


    private onDragStart(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        this.props.dispatch({
            type    : 'ImageAction/DragStart',
            event   : _e,
        })
    }

    private onDragEnd(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        
        this.props.dispatch({
            type    : 'ImageAction/DragEnd',
            event   : _e,
        });
        _e.stopPropagation();
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(NewImage)
