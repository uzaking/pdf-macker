import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
    ListImagePropsInterface, initialState
} from '../../reducers/_Image/ListImage'

export class ListImage
    extends React.Component<ListImagePropsInterface, {}>
{
    componentDidMount()
    {
        this.props.dispatch({
            type    : 'ImageAction/loadAllImage'
        });
    }
    render() {
        const ll = (this.props.ListImage) ?
                    this.props.ListImage : initialState;
        return (
            <div className="container">
                <Link to="/image/new" className="large_link">新規</Link>
                <div className="d-flex">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>イメージ</th>
                                <th>詳細</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.buildImageList(ll) }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
    private buildImageList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: any, key) => {
            console.log(val);
            return (
                <tr key={key}>
                    <td rowSpan={7}>
                        <img src={val.image} alt="画像" width="100px" height=""></img>
                    </td>
                    <td>
                        <tr>
                            <td width="150px">メモ：</td>
                            <td>{val.memo}</td>
                        </tr>
                        <tr>
                            <td colSpan={2}>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm"
                                    data-toggle="modal" data-target='#show_qr'
                                    onClick={
                                        () => {
                                            this.props.dispatch({
                                                type    : 'EditImage/set',
                                                template: val
                                            });
                                        }
                                }>
                                    <Link
                                        to="/image/edit"
                                        className="large_link button_link"
                                        >編集</Link></button>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm"
                                    data-toggle="modal" data-target='#show_qr'
                                    onClick={
                                        () => {
                                            this.props.dispatch({
                                                type    : 'ImageAction/del',
                                                id      : val.id
                                            });
                                        }
                                }>削除</button>
                            </td>
                        </tr>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListImage)
