import React from 'react';
import { connect } from 'react-redux';

import {
    ListImagePropsInterface, initialState
} from '../../reducers/_Image/ListImage'

interface ImagePropsInterface extends ListImagePropsInterface
{
    next    : string,
    initial? : number,
}

const initialImage = {
    ...initialState,
    ...{
        next    : '',
        prop    : '',
        initial : 1,
    }
}


export class SelectImage
    extends React.Component<ImagePropsInterface, {}>
{
    componentWillMount()
    {
        this.props.dispatch({
            type    : 'ImageAction/loadAllImage'
        });
    }
    render() {
        const ll = (this.props.ListImage) ?
                    this.props.ListImage : initialImage;
        return (
            <div className="container">
                <div className="d-flex">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>イメージ</th>
                                <th>詳細</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.buildImageList(ll) }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
    private buildImageList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: any, key) => {
            return (
                <tr key={key}>
                    <td rowSpan={7}>
                        <img src={val.image} alt="画像" width="100px" height=""></img>
                    </td>
                    <td>
                        <table>
                            <tbody>
                            <tr>
                                <td width="150px">メモ：</td>
                                <td>{val.memo}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>
                                    <button
                                        type="button"
                                        className="btn btn-secondary btn-sm"
                                        data-toggle="modal" data-target='#show_qr'
                                        onClick={
                                            () => {
                                                this.props.dispatch({
                                                    type    : this.props.next,
                                                    image   : val
                                                });
                                            }
                                    }>Add</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(SelectImage)
