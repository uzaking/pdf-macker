import React from 'react';
import { connect } from 'react-redux';

import {
    EditImagePropsInterface, initialState
} from '../../reducers/_Image/EditImage'

interface ImagePropsInterface extends EditImagePropsInterface
{
    next?    : string,
    width?   : number,
    height?  : number,
    viewbox? : string,
    key?     : number,
}

const initialImage = {
    ...initialState,
    ...{
        next    : '',
        width   : 100,
        height  : 100,
        viewbox : '0 0 100 100',
        key     : 0,
    }
}

export class EditImage
    extends React.Component<ImagePropsInterface, {}>
{
    private point: number[] = [0,0,0,0];

    render() {
        const ei = (this.props.EditImage) ?
                    this.props.EditImage : initialImage;
        if (ei.image === '') return;
        this.calcPoint(ei);
        
        return (
        <svg
            width={ this.props.width }
            height={ this.props.height }
            viewBox={ this.props.viewbox }
            onMouseMove={(e) => {
            }}
            onMouseUp={() => {
            }}
        >
            <image
                x={ ei.x } y={ ei.y }
                width={ ei.width }
                height={ ei.height }
                href={ ei.image }
                onMouseDown={(e) => {
                    
                }}
            ></image>
            <path
                d={"M " + this.point[0] + " " + this.point[1] + " h 20 v 20 h -20 v -20"}
                stroke="white" stroke-width="1" fill="gray" className="ImagePointer"/>
            <path
                d={"M " + this.point[2] + " " + this.point[1] + " h 20 v 20 h -20 v -20"}
                stroke="white" stroke-width="1" fill="gray" className="ImagePointer"/>
            <path
                d={"M " + this.point[2] + " " + this.point[3] + " h 20 v 20 h -20 v -20"}
                stroke="white" stroke-width="1" fill="gray" className="ImagePointer"/>
            <path
                d={"M " + this.point[0] + " " + this.point[3] + " h 20 v 20 h -20 v -20"}
                stroke="white" stroke-width="1" fill="gray" className="ImagePointer"/>
        </svg>
        );
    }

    private calcPoint(ei: any): void
    {
        this.point[0] = ei.x - 10;
        this.point[1] = ei.y - 10;
        this.point[2] = ei.x + ei.width - 10;
        this.point[3] = ei.y + ei.height - 10;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(EditImage)
