import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// import reducer
import {
    ListTemplatePropsInterface, initialState
} from '../../reducers/_Template/ListTemplate'
import {
    NewPrintPropsInterface, initialState as printInitial
} from '../../reducers/_Print/NewPrint';

// import component
import NewPrint from '../Print/NewPrint';

interface LayoutPropsInterface extends ListTemplatePropsInterface, NewPrintPropsInterface {};

export class ListLayoutExcell
    extends React.Component<LayoutPropsInterface, {}>
{
    private print: boolean  = false;

    componentWillMount(){
        this.props.dispatch({
            type    : 'TemplateAction/initialLoad'
        });
    }
    render(): any {
        const ll = (this.props.ListTemplate) ?
                    this.props.ListTemplate : initialState;
        const np = (this.props.NewPrint) ?
                    this.props.NewPrint : printInitial;
        return (
            <div className="container">
                <Link
                    to="/layout_excell/new"
                    className="large_link">新規</Link>
                <div className="d-flex">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>イメージ</th>
                                <th>詳細</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.buildLayoutList(ll) }
                        </tbody>
                    </table>
                </div>
                <div>
                    { (np.show) ? this.showPrinter() : <p></p> }
                </div>
            </div>
        );
    }
    private showPrinter(): any
    {
        return (
            <div className="component flow_window">
                <button
                    type="button"
                    className="btn btn-secondary btn-xl"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={() => {
                        this.props.dispatch({
                            type    : 'NewPrint/setShow',
                            show    : false,
                        })
                    }}
                >閉じる</button>
                <NewPrint />
            </div>
        );
    }
    private buildLayoutList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: any, key) => {
            if (val.sheet === '') {
                return
            }
            return (
                <tr key={key}>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td width="150px">名前：</td>
                                    <td>{val.name}</td>
                                </tr>
                                <tr>
                                    <td width="150px">メモ：</td>
                                    <td>{val.memo}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <button
                                            type="button"
                                            className="btn btn-warning btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={() => {
                                                this.props.dispatch({
                                                    type    : 'LayoutAction/changeExcellTemplate',
                                                    template: val,
                                                })
                                            }}
                                        >
                                            <Link
                                                to="/layout_excell/new">印刷</Link>
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-warning btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={() => {
                                                this.props.dispatch({
                                                    type    : 'PrintAction/exportCSV',
                                                    template: val,
                                                })
                                            }}
                                        >変数出力
                                        </button>
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={
                                                () => {
                                                    this.props.dispatch({
                                                        type    : 'LayoutAction/del',
                                                        id      : val.id,
                                                        key     : key,
                                                    });
                                                }
                                        }>削除</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListLayoutExcell)
