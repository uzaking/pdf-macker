import React from 'react';
import { connect } from 'react-redux';

// import component
import SelectTemplate from '../Template/SelectTemplate';
import ListText from '../Text/ListText';
import SelectImage from '../Image/SelectImage';

// import reducer
import {
    ExcellLayoutPropsInterface, initialState
} from '../../reducers/_Layout/ExcellLayout'


export class NewlayoutExcell
    extends React.Component<ExcellLayoutPropsInterface, {}>
{
    private tabCounter: number = 2;

    render() {
        const nl = (this.props.ExcellLayout) ?
                    this.props.ExcellLayout : initialState;
        if (nl.done){
            window.location.href = nl.back;
        }
        return (
            <div className="container">
                <nav className="navbar">
                    <h6>New Layout</h6>
                    <div className="ToolBox">

                    </div>
                    <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        data-toggle="modal" data-target='#show_qr'
                        onClick={
                            () => {
                                this.props.dispatch({
                                    type    : 'NewPrint/set',
                                    layout  : nl
                                });
                                this.props.dispatch({
                                    type    : 'PrintAction/export',
                                });
                            }
                    }>印刷</button>
                </nav>
                <div className="LayoutBase">
                    { this.showTexts(nl.contents) }
                </div>
                <div className="LayoutSub">
                    <div className="container">
                        <div
                            id="dragtarget" className="dragTest center"
                            onDragOver={(e) => this.onDragStart(e)}
                            onDrop={(e) => this.onDragEnd(e)}>
                                CSVファイルをドラッグ
                        </div>
                    </div>
                    <ul className="nav nav-tabs" role="tablist">
                        <li className="nav-item">
                            <a
                                className="nav-link active" id="item1-tab" href="#item1"
                                >画像
                            </a>
                        </li>
                    </ul>
                    <div className="tab-content">
                        <div className="tab-pane fade" id="item1">
                            <div className="container LayoutGroup">
                                <SelectImage next="TextAction/addImage" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

    private showTexts(texts): any
    {
        console.log(texts);
        if (texts.length <= 1 ) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = Object.keys(texts).map((val: any, key) => {
            return (
                <div key={key} className='d-flex'>
                    <div className="my-box col TextTitle2">
                        {val}
                    </div>
                    <div className="my-box col">
                        <input
                            className="form-control TextInputMiddle"
                            type="text"
                            defaultValue={texts[val]}
                            onChange={(e) => {
                                this.props.dispatch({
                                    type    : 'ExcellLayout/updateContents',
                                    key     : key,
                                    content : e.target.value
                                });
                            }}/>
                    </div>
                    <br></br>
                </div>
            );
        });
        return _lists;
    }

    private onDragStart(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        this.props.dispatch({
            type    : 'DragAction/DragStart',
            event   : _e,
        })
    }

    private onDragEnd(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        
        this.props.dispatch({
            type    : 'DragAction/DragEndCSV',
            event   : _e,
            next    : 'LayoutAction/atachCSV'
        });
        _e.stopPropagation();
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(NewlayoutExcell)
