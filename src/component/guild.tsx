import React from 'react';
import { Provider } from 'react-redux'

// import 'bootstrap/dist/css/bootstrap.css';

import LoadingAnimation from '../animation/loading.animation';
import ToastrAnimation from '../animation/toastr.animation';

// import rootReducer from './reducers'
import { createStore } from '../store/configureStore';

const store = createStore();
interface AppPropsInterface {
  dispatch?: any;
}

export default class Guild
  extends React.Component <AppPropsInterface, {}>
{
  render() {
    return (
      <Provider store={ store }>
        <div>
          <h3>Welcome guild</h3>
        </div>
        <LoadingAnimation />
        <ToastrAnimation />
      </Provider>
    )
  }
}
// render (
//    <App />,
//    document.getElementById('regist')
//);