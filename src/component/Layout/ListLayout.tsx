import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

// import reducer
import {
    ListLayoutPropsInterface, initialState
} from '../../reducers/_Layout/ListLayout'
import {
    NewPrintPropsInterface, initialState as printInitial
} from '../../reducers/_Print/NewPrint';

// import component
import NewPrint from '../Print/NewPrint';

interface LayoutPropsInterface extends ListLayoutPropsInterface, NewPrintPropsInterface {};

export class ListLayout
    extends React.Component<LayoutPropsInterface, {}>
{
    private print: boolean  = false;

    componentWillMount(){
        this.props.dispatch({
            type    : 'LayoutAction/initialLoad'
        });
        this.props.dispatch({
            type    : 'TemplateAction/initialLoad'
        });
        this.props.dispatch({
            type    : 'ImageAction/initialLoad'
        });
    }
    render(): any {
        const ll = (this.props.ListLayout) ?
                    this.props.ListLayout : initialState;
        const np = (this.props.NewPrint) ?
                    this.props.NewPrint : printInitial;
        return (
            <div className="container">
                <Link to="/layout/new" className="large_link">新規</Link>
                <div className="d-flex">
                    <table className="table">
                        <thead>
                            <tr>
                                <th>イメージ</th>
                                <th>詳細</th>
                            </tr>
                        </thead>
                        <tbody>
                            { this.buildLayoutList(ll) }
                        </tbody>
                    </table>
                </div>
                <div>
                    { (np.show) ? this.showPrinter() : <p></p> }
                </div>
            </div>
        );
    }
    private showPrinter(): any
    {
        return (
            <div className="component flow_window">
                <button
                    type="button"
                    className="btn btn-secondary btn-xl"
                    data-toggle="modal" data-target='#show_qr'
                    onClick={() => {
                        this.props.dispatch({
                            type    : 'NewPrint/setShow',
                            show    : false,
                        })
                    }}
                >閉じる</button>
                <NewPrint />
            </div>
        );
    }
    private buildLayoutList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: any, key) => {
            return (
                <tr key={key}>
                    <td>
                        <img src={val.svg} alt="画像" width="200px" height=""></img>
                    </td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td width="150px">名前：</td>
                                    <td>{val.name}</td>
                                </tr>
                                <tr>
                                    <td width="150px">テンプレート：</td>
                                    <td>{val.template}</td>
                                </tr>
                                <tr>
                                    <td width="150px">メモ：</td>
                                    <td>{val.memo}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <button
                                            type="button"
                                            className="btn btn-primary btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={() => {
                                                this.props.dispatch({
                                                    type    : 'PrintAction/setup',
                                                    layout  : val,
                                                })
                                                this.props.dispatch({
                                                    type    : 'NewPrint/setShow',
                                                    show    : true,
                                                })
                                            }}
                                        >印刷</button>
                                        <button
                                            type="button"
                                            className="btn btn-info btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={
                                                () => {
                                                    this.props.dispatch({
                                                        type    : 'LayoutAction/edit',
                                                        layout  : val,
                                                        key     : key,
                                                    });
                                                }
                                        }>
                                            <Link
                                                to="/layout/edit"
                                                className="large_link button_link"
                                                >編集</Link></button>
                                        <button
                                            type="button"
                                            className="btn btn-secondary btn-sm"
                                            data-toggle="modal" data-target='#show_qr'
                                            onClick={
                                                () => {
                                                    this.props.dispatch({
                                                        type    : 'LayoutAction/del',
                                                        id      : val.id,
                                                        key     : key,
                                                    });
                                                }
                                        }>削除</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListLayout)
