import React from 'react';
import { connect } from 'react-redux';

// import component
import SelectPaperSize from '../_helper/SelectPaperSize';
import SelectPintOrientation from '../_helper/SelectPintOrientation';

// import reducer
import {
    NewTemplatePropsInterface, initialState
} from '../../reducers/_Template/NewTemplate'

export class Newlayout
    extends React.Component<NewTemplatePropsInterface, {}>
{
    componentWillMount(){
        this.props.dispatch({
            type    : 'GroupAction/initialLoad'
        });
    }
    render() {
        const nt = (this.props.NewTemplate) ?
                    this.props.NewTemplate : initialState;
        if (nt.done) {
            this.doneProcess();
        }
        return (
            <div className="container">
                <nav className="navbar navbar-dark">
                    <h6>New Template</h6>
                </nav>

                <div className="form-group row">
                    <label
                        htmlFor="template-title"
                        className="col-sm-2
                        col-form-label">な</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            id="template-title"
                            className="form-control"
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'NewTemplate/setName',
                                    name    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    { (nt.sheet !== '') ?  <p></p> : <SelectPaperSize next='NewTemplate/setPaper' />}
                </div>
                <br></br>
                <div className="form-group row">
                { (nt.sheet !== '') ?  <p></p> : <SelectPintOrientation next='NewTemplate/setOrientation' />}
                </div>
                <br></br>
                <div className="form-group row">
                    <div>
                        {
                            (nt.svg === '')
                                ? <p></p> : <img src={nt.svg} alt="てんぷる" width="200px"></img>
                        }
                    </div>
                    <div
                        id="dragtarget" className="dragTest center"
                        onDragOver={(e) => this.onDragStart(e)}
                        onDrop={(e) => this.onDragEnd(e)}>
                            { this.checkFile(nt.svg, nt.sheet) }
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        onClick={
                            () => this.props.dispatch({
                                type            : 'TemplateAction/saveNewTemplate',
                            })
                        }>
                        登録</button>
                </div>
            </div>
        );
    }

    private checkFile(svg: string, sheet: string): string
    {
        if (svg.length > 0) {
            return 'SVGファイル追加';
        }
        if (sheet.length > 0) {
            return 'Excellファイル追加';
        }
        return 'ここにファイルをドラッグ';
    }

    private doneProcess(): void
    {
        this.props.dispatch({
            type    : 'NewTemplate/reset'
        });
        window.location.href = '/template';
    }

    private onDragStart(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        this.props.dispatch({
            type    : 'DragAction/DragStart',
            event   : _e,
            target  : 'dragtarget',
        })
    }

    private onDragEnd(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        
        this.props.dispatch({
            type    : 'DragAction/DragEnd',
            event   : _e,
            next    : 'NewTemplate'
        });
        _e.stopPropagation();
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(Newlayout)
