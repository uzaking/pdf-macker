import React from 'react';
import { connect } from 'react-redux';

// import component
import SelectPaperSize from '../_helper/SelectPaperSize';
import SelectPintOrientation from '../_helper/SelectPintOrientation';

// import reducer
import {
    EditTemplatePropsInterface, initialState
} from '../../reducers/_Template/EditTemplate'

export class EditTemplate
    extends React.Component<EditTemplatePropsInterface, {}>
{
    render() {
        const et = (this.props.EditTemplate) ?
                    this.props.EditTemplate : initialState;

        this.checkEditEnd(et.done);

        return (
            <div className="container">
                <nav className="navbar navbar-dark">
                    <h6>New Template</h6>
                </nav>

                <div className="form-group row">
                    <label
                        htmlFor="template-title"
                        className="col-sm-2
                        col-form-label">な</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            id="template-title"
                            className="form-control"
                            value={ et.name }
                            onChange={
                                (e:any) => this.props.dispatch({
                                    type    : 'EditTemplate/setName',
                                    name    : e.target.value,
                                })
                            } />
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <SelectPaperSize next='EditTemplate/setPaper' />
                </div>
                <br></br>
                <div className="form-group row">
                    <SelectPintOrientation next='EditTemplate/setOrientation' />
                </div>
                <br></br>
                <div className="form-group row">
                    <div className="col">
                        <img src={et.svg} alt="てんぷれーと" width="100px"></img>
                    </div>
                    <div
                        id="dragtarget" className="dragTest center col"
                        onDragOver={(e) => this.onDragStart(e)}
                        onDrop={(e) => this.onDragEnd(e)}>
                            { this.checkSvg(et.svg) }
                    </div>
                </div>
                <br></br>
                <div className="form-group row">
                    <button
                        type="button"
                        className="btn btn-secondary btn-sm"
                        onClick={
                            () => this.props.dispatch({
                                type            : 'TemplateAction/updateTemplate',
                            })
                        }>
                        更新</button>
                </div>
            </div>
        );
    }

    private checkEditEnd(end: boolean): void
    {
        if (end) {
            window.location.href = "/template";
            this.props.dispatch({
                type    : 'EditTemplate/reset'
            });
        }
    }

    private checkSvg(svg: string): string
    {
        if (svg.length > 0) {
            return '追加済み';            
        }
        return 'ここにSVGファイルをドラッグ';
    }

    private onDragStart(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();
        this.props.dispatch({
            type    : 'TemplateAction/DragStart',
            event   : _e,
        })
    }

    private onDragEnd(e: any): void
    {
        const _e = e as Event;
        _e.preventDefault();

        this.props.dispatch({
            type    : 'TemplateAction/DragEnd',
            event   : _e,
            next    : 'EditTemplate'
        });
        _e.stopPropagation();
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(EditTemplate)
