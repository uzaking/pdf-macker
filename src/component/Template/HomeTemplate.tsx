import React from 'react';
import { Provider } from 'react-redux'
import { Link } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.css';
import '../_style/app.scss';

import LoadingAnimation from '../../animation/loading.animation';
import ToastrAnimation from '../../animation/toastr.animation';

// import Components
import ListTemplate from './ListTemplate';
import NewTemplate from './NewTemplate';
import EditTemplate from './EditTemplate';

// import rootReducer from './reducers'
import { createStore } from '../../store/configureStore';


const store = createStore();
interface AppPropsInterface {
  dispatch? : any;
  page      : string
}

export default class HomeTemplate
  extends React.Component <AppPropsInterface, {}>
{
  render() {
    return (
      <Provider store={ store }>
        <nav className="navbar navbar-dark bg-dark">
          <Link to="/template" className="large_link navbar-brand">Template</Link>
          <Link to="/" className="large_link navbar-brand">Home</Link>
         </nav>
        <div className="container-fluid">
            <div className="container">
              { this.checkPage(this.props.page) }
            </div>
        </div>
        <LoadingAnimation />
        <ToastrAnimation />
      </Provider>
    )
  }
  private checkPage(page: string): any
  {
    if (page === 'new') {
      return <NewTemplate />;
    }
    if (page === 'edit') {
      return <EditTemplate />;
    }
    return <ListTemplate />;
  }
}
// render (
//    <App />,
//    document.getElementById('regist')
//);