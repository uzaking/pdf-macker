import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
    ListTemplatePropsInterface, initialState
} from '../../reducers/_Template/ListTemplate'
import { EditTemplateInterface } from '../../reducers/_Template/_template_interface';

export class ListTemplate
    extends React.Component<ListTemplatePropsInterface, {}>
{
    componentWillMount(){
        this.props.dispatch({
            type    : 'TemplateAction/initialLoad'
        });
        this.props.dispatch({
            type    : 'GroupAction/initialLoad'
        });
    }
    render() {
        const lt = (this.props.ListTemplate) ?
                    this.props.ListTemplate : initialState;
        return (
            <div className="container">
                <Link to="/template/new" className="btn btn-info">追加</Link>

                <div className="d-flex">
                <table className="table">
                    <thead>
                        <tr>
                            <th>イメージ</th>
                            <th>詳細</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.buildTemplateList(lt) }
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
    private buildTemplateList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: EditTemplateInterface, key) => {
            console.log(val);
            return (
                <tr key={key}>
                    <td rowSpan={1}>
                        <img src={val.svg} alt="てんぷく" width="" height="150px"></img>
                    </td>
                    <td>
                        <tr>
                            <td width="150px">名前：</td>
                            <td>{val.name}</td>
                        </tr>
                        <tr>
                            <td width="150px">用紙サイズ：</td>
                            <td>{val.paper}</td>
                        </tr>
                        <tr>
                            <td width="150px">印刷向き：</td>
                            <td>{val.orientation}</td>
                        </tr>
                        <tr>
                            <td width="150px">メモ：</td>
                            <td>{val.memo}</td>
                        </tr>
                        <tr>
                            <td colSpan={2}>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm"
                                    data-toggle="modal" data-target='#show_qr'
                                    onClick={
                                        () => {
                                            this.props.dispatch({
                                                type    : 'EditTemplate/set',
                                                template: val
                                            });
                                        }
                                }>
                                    <Link
                                        to="/template/edit"
                                        className="large_link button_link"
                                        >編集</Link></button>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm"
                                    data-toggle="modal" data-target='#show_qr'
                                    onClick={
                                        () => {
                                            this.props.dispatch({
                                                type    : 'TemplateAction/del',
                                                id      : val.id
                                            });
                                        }
                                }>削除</button>
                            </td>
                        </tr>
                    </td>
                </tr>
            );
        });
        return _lists;
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(ListTemplate)
