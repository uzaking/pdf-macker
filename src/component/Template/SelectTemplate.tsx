import React from 'react';
import { connect } from 'react-redux';

// import reducers
import {
    ListTemplatePropsInterface, initialState
} from '../../reducers/_Template/ListTemplate'
import {
    EditTemplateInterface
} from '../../reducers/_Template/_template_interface';

// Propsの拡張
interface TemplatePropsInterface extends ListTemplatePropsInterface
{
    next    : string,
    filter? : string,
}

const initialTemplate = {
    ...initialState,
    ...{
        next    : '',
        filter  : '',
    }
}

export class SelectTemplate
    extends React.Component<TemplatePropsInterface, {}>
{
    componentWillMount(): void
    {
        this.props.dispatch({
            type    : 'TemplateAction/initialLoad'
        });
    }
    render() {
        const lt = (this.props.ListTemplate) ?
                    this.props.ListTemplate : initialTemplate;
        return (
            <div className="d-flex">
                <table className="table">
                    <thead>
                        <tr>
                            <th>イメージ</th>
                            <th>詳細</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.buildTemplateList(lt) }
                    </tbody>
                </table>
            </div>
        );
    }
    private buildTemplateList(lt: any): any
    {
        if (lt === initialState) {
            return (<tr><td>登録なし</td></tr>)
        }
        const _lists = lt.map((val: EditTemplateInterface, key) => {
            return (this.checkFilter(val.svg, val.sheet))
                        ? this.showTemplate(val, key) : <tr key={key}></tr>;
        });
        return _lists;
    }
    private checkFilter(svg: string, sheet: string): boolean
    {
        if (this.props.filter === '' || this.props.filter === undefined) {
            return true;
        }
        if (this.props.filter === 'svg' && svg !== '') {
            return true;
        } else if (this.props.filter === 'svg' && svg === '') {
            return false;
        }
        if (this.props.filter === 'excell' && sheet !== '') {
            return true;
        } else if(this.props.filter === 'excell' && sheet === '') {
            return false;
        }
        return false;
    }
    private showTemplate(template: any, key: number): any
    {
        return (
            <tr key={key}>
                <td rowSpan={1}>
                    <img src={template.svg} alt="てんぷ羅" width="70px" height=""></img>
                </td>
                <td>
                    <table>
                        <tbody>
                        <tr>
                            <td width="150px">名前：</td>
                            <td>{template.name}</td>
                        </tr>
                        <tr>
                            <td width="150px">サイズ：</td>
                            <td>{template.paper}</td>
                        </tr>
                        <tr>
                            <td width="150px">向き：</td>
                            <td>{template.orientation}</td>
                        </tr>
                        <tr>
                            <td colSpan={2}>
                                <button
                                    type="button"
                                    className="btn btn-secondary btn-sm"
                                    data-toggle="modal" data-target='#show_qr'
                                    onClick={
                                        () => {
                                            this.props.dispatch({
                                                type    : this.props.next,
                                                template: template
                                            });
                                        }
                                }>Select</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        );
    }
}

export default connect(
    (state: any) => {
        return state;
    }
)(SelectTemplate)
