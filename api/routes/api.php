<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('add_keys', [App\Http\Controllers\UsersController::class, 'addUser']);
Route::get('add_keys', [App\Http\Controllers\TemplateController::class, 'addInitialUser']);
Route::get('load_initials', [App\Http\Controllers\TemplateController::class, 'getInitialUsers']);
Route::get('quest_pubkey', [App\Http\Controllers\TemplateController::class, 'getPubkey']);

Route::group(['prefix' => 'template', 'as' => 'template.'], function() {
    Route::post('new', [App\Http\Controllers\TemplateController::class, 'add']);
    Route::post('update', [App\Http\Controllers\TemplateController::class, 'update']);
    Route::post('delete', [App\Http\Controllers\TemplateController::class, 'delete']);
    Route::get('get', [App\Http\Controllers\TemplateController::class, 'get']);
    Route::get('get/group1', [App\Http\Controllers\TemplateController::class, 'getByGroup1']);
    Route::get('get/group2', [App\Http\Controllers\TemplateController::class, 'getByGroup2']);
    Route::get('get/group3', [App\Http\Controllers\TemplateController::class, 'getByGroup3']);
    Route::get('get/all', [App\Http\Controllers\TemplateController::class, 'getAll']);
});

Route::group(['prefix' => 'layout', 'as' => 'layout.'], function() {
    Route::post('new', [App\Http\Controllers\LayoutController::class, 'add']);
    Route::post('update', [App\Http\Controllers\LayoutController::class, 'update']);
    Route::post('delete', [App\Http\Controllers\LayoutController::class, 'delete']);
    Route::get('get', [App\Http\Controllers\LayoutController::class, 'get']);
    Route::get('get/group1', [App\Http\Controllers\LayoutController::class, 'getByGroup1']);
    Route::get('get/group2', [App\Http\Controllers\LayoutController::class, 'getByGroup2']);
    Route::get('get/group3', [App\Http\Controllers\LayoutController::class, 'getByGroup3']);
    Route::get('get/all', [App\Http\Controllers\LayoutController::class, 'getAll']);
});

Route::group(['prefix' => 'group', 'as' => 'group.'], function() {
    Route::post('1/new', [App\Http\Controllers\Group1Controller::class, 'add']);
    Route::post('1/update', [App\Http\Controllers\Group1Controller::class, 'change']);
    Route::post('1/delete', [App\Http\Controllers\Group1Controller::class, 'del']);

    Route::get('1/get', [App\Http\Controllers\Group1Controller::class, 'getById']);
    Route::get('1/get/all', [App\Http\Controllers\Group1Controller::class, 'getAll']);

    Route::post('2/new', [App\Http\Controllers\Group2Controller::class, 'add']);
    Route::post('2/update', [App\Http\Controllers\Group2Controller::class, 'change']);
    Route::post('2/delete', [App\Http\Controllers\Group2Controller::class, 'del']);

    Route::get('2/get', [App\Http\Controllers\Group2Controller::class, 'getById']);
    Route::get('2/get/all', [App\Http\Controllers\Group2Controller::class, 'getAll']);

    Route::post('3/new', [App\Http\Controllers\Group3Controller::class, 'add']);
    Route::post('3/update', [App\Http\Controllers\Group3Controller::class, 'change']);
    Route::post('3/delete', [App\Http\Controllers\Group3Controller::class, 'del']);

    Route::get('3/get', [App\Http\Controllers\Group3Controller::class, 'getById']);
    Route::get('3/get/all', [App\Http\Controllers\Group3Controller::class, 'getAll']);
});

Route::group(['prefix' => 'image', 'as' => 'image.'], function() {
    Route::post('new', [App\Http\Controllers\ImageController::class, 'add']);
    Route::post('update', [App\Http\Controllers\ImageController::class, 'change']);
    Route::post('delete', [App\Http\Controllers\ImageController::class, 'del']);

    Route::get('get', [App\Http\Controllers\ImageController::class, 'getById']);
    Route::get('get/all', [App\Http\Controllers\ImageController::class, 'getAll']);
});

Route::group(['prefix' => 'print', 'as' => 'print.'], function() {
    Route::post('new', [App\Http\Controllers\UsersController::class, 'addUser']);
    Route::post('update', [App\Http\Controllers\UsersController::class, 'getInitialUsers']);
    Route::post('delete', [App\Http\Controllers\UsersController::class, 'getPubkey']);

    Route::get('get', [App\Http\Controllers\UsersController::class, 'getProgress']);
    Route::get('get/all', [App\Http\Controllers\UsersController::class, 'getReport']);
});

Route::group(['prefix' => 'data', 'as' => 'data.'], function() {
    Route::get('get1', [App\Http\Controllers\DataController::class, 'export1']);
    Route::get('get2', [App\Http\Controllers\DataController::class, 'export2']);
    Route::get('get3', [App\Http\Controllers\DataController::class, 'export3']);
});

