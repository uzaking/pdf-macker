<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request', function (Blueprint $table) {
            $table->id();
            $table->string('request_id');
            $table->string('order_id');
            $table->string('permit_id');
            $table->string('detail_id');
            $table->string('users_id');
            $table->string('reward_id');
            $table->string('photo_id');
            $table->string('title');
            $table->integer('start');
            $table->integer('post_limit');
            $table->integer('completion_limit');
            $table->integer('request_flag');
            $table->integer('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request');
    }
}
