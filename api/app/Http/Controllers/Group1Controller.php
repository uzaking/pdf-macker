<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group1;

class Group1Controller extends Controller
{
    public function add(Request $request)
    {
        if (Group1::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        if (Group1::update($request)) {
            return true;
        }
        return false;
    }

    public function delete(Request $request)
    {
        if (Group1::delete($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Group1::getById($request);
    }

    public function getAll(Request $request)
    {
        return Group1::getAll($request);
    }
}
