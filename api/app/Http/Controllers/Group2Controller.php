<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group2;

class Group2Controller extends Controller
{
    public function add(Request $request)
    {
        if (Group2::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        if (Group2::update($request)) {
            return true;
        }
        return false;
    }

    public function delete(Request $request)
    {
        if (Group2::delete($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Group2::getById($request);
    }

    public function getAll(Request $request)
    {
        return Group2::getAll($request);
    }
}
