<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Layout\Layout;

class LayoutController extends Controller
{
    public function add(Request $request)
    {
        if (Layout::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        if (Layout::change($request)) {
            return true;
        }
        return false;
    }

    public function delete(Request $request)
    {
        if (Layout::del($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Layout::getById($request);
    }

    public function getAll(Request $request)
    {
        return Layout::getAll($request);
    }

    public function getByTemplate(Request $request)
    {
        return Layout::getByTemplate($request['template_id']);
    }

    public function getByGroup1(Request $request)
    {
        return Layout::getByGroup1($request['group_id']);
    }
    public function getByGroup2(Request $request)
    {
        return Layout::getByGroup2($request['group_id']);
    }
    public function getByGroup3(Request $request)
    {
        return Layout::getByGroup3($request['group_id']);
    }
}
