<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Template\Template;

class TemplateController extends Controller
{
    public function add(Request $request)
    {
        if (Template::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        try {
            Template::change($request);
            return array('result' => true);
        } catch (\Throwable $th) {
            return array('result' => false);
        }
    }

    public function delete(Request $request)
    {
        if (Template::del($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Template::getById($request['id']);
    }

    public function getAll(Request $request)
    {
        return Template::getAll();
    }
}
