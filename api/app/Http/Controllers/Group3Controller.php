<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group3;

class Group3Controller extends Controller
{
    public function add(Request $request)
    {
        if (Group3::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        if (Group3::update($request)) {
            return true;
        }
        return false;
    }

    public function delete(Request $request)
    {
        if (Group3::delete($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Group3::getById($request);
    }

    public function getAll(Request $request)
    {
        return Group3::getAll($request);
    }
}
