<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image\Image;

class ImageController extends Controller
{
    public function add(Request $request)
    {
        if (Image::add($request)) {
            return true;
        }
        return false;
    }

    public function update(Request $request)
    {
        if (Image::update($request)) {
            return true;
        }
        return false;
    }

    public function delete(Request $request)
    {
        if (Image::delete($request)) {
            return true;
        }
        return false;
    }

    public function get(Request $request)
    {
        return Image::getById($request);
    }

    public function getAll(Request $request)
    {
        return Image::getAll($request);
    }

}
