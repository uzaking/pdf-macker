<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SVG\SVG;

class SVGController extends Controller
{
    /**
     * @param Request $request
     */
    public function addSVG(Request $request)
    {
        // dump($request);
        // return $request;
        return SVG::addSVG($request);
    }

    // ユーザー詳細取得
    public function getSVG(Request $request): array
    {
        return SVG::getSVGById($request->id);
    }

    public function updateSVG(Request $request)
    {
        return SVG::updateSVG($request);
    }
}
