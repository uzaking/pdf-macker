<?php

namespace App\Http\Helper;

class ReportHelper
{
    public static $FlagTable = array(
        'AddReport'         => 100100,
        'ReviewManager'     => 100110,
        'ReviewClient'      => 100101,
        'ReviewedReport'    => 100111,
    );

    public static function getFlag(string $mode): int
    {
        return self::$FlagTable[$mode];
    }


    public static function getRand(int $length = 20): string
    {
        return substr(base_convert(hash('sha256', uniqid()), 16, 36), 0, $length);
    }
}

