<?php

namespace App\Http\Helper;

use App\_lib\RSA\RSA;

class RSAHelper
{

    /**
     * RSA鍵ペア作成
     *
     * @param [type] $val
     * @return void
     */
    public static function buildRSAKey($val)
    {
        $RR = RSA::RSARepository();
        return $RR
            ->buildNewRSAKeysMulti($val['count'])
            ->getRSAKeys();
    }

    /**
     * 秘密鍵で署名されたシグネチャの検証
     *
     * @param [type] $val
     * @return void
     */
    public static function verify($val)
    {
        $RR = RSA::RSARepository();
        $_val = $RR
            ->addRSAKey($val['public'], $val['private'], 2048)
            ->verify($val['check'], $val['challenge']);

        if ($_val == 1) {
            return 'true';
        }
        return $_val;
    }    

    public static function verifyTest($val)
    {
        $RR = RSA::RSARepository();
        $_val = $RR
            ->addRSAKey($val['public'], $val['private'], 2048)
            ->privateDecrypt($val['challenge'])
            ->getResult();

        if ($_val == $val['check']) {
            return 'UNKO';
        }
        return array(
            $_val,
            $val['challenge']
        );
    }
}

