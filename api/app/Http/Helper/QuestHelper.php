<?php

namespace App\Http\Helper;

class QuestHelper
{
    public static $FlagTable = array(
        'AddQuest'          => 100001,
        'OpenQuest'         => 100011,
        'ProgressQuest'     => 100111,
        'CancelQuest'       => 100191,
        'CloselQuest'       => 200191,
        'ReportQuest'       => 100211,
        'ReviewContoractor' => 100311,
        'ReviewClient'      => 100313,
        'ReviewedQuest'     => 100333,
        'RewardQuest'       => 100343,
    );

    public static function getFlag(string $mode): int
    {
        return self::$FlagTable[$mode];
    }


    public static function getRand(int $length = 20): string
    {
        return substr(base_convert(hash('sha256', uniqid()), 16, 36), 0, $length);
    }

    /**
     * 1：発注者フラグ
     * 10：管理者フラグ
     * 100：冒険者フラグ
     *
     * @param object $quest
     * @return integer
     */
    public static function getQuestFlag(object $quest): int
    {
        $flag = 100001;
        if ($quest['manager'] !== '' && $quest['manager_sigunature'] !== '') {
            $flag = $flag + 10;
        }
        if ($quest['contractor'] !== '' && $quest['contractor_sigunature'] !== '') {
            $flag = $flag + 100;
        }
        return $flag;
    }

    public static function getQuestFlag2(int $flag): int
    {
        $val = 100100 - $flag;
        if ($val === 0) return 200;
        if ($flag === 10) return 20;
        if ($flag === 1) return 2;
        if ($flag === 11) return 0;
    }

    /**
     * 1：発注者フラグ
     * 10：管理者フラグ
     * 100：冒険者フラグ
     *
     * @param object $quest
     * @return integer
     */
    public static function getReportFlag(object $quest): int
    {
        $flag = 100100;
        if ($quest['manager'] !== '' && $quest['manager_sigunature'] !== '') {
            $flag = $flag + 10;
        }
        if ($quest['client'] !== '' && $quest['client_sigunature'] !== '') {
            $flag = $flag + 1;
        }
        return $flag;
    }
}

