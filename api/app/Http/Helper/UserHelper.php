<?php

namespace App\Http\Helper;

class UserHelper
{
    private static $UserHelper;

    private $InitialUser     = array();
    private $InitialCard     = array();
    private $InitialFace     = array();
    private $InitialDetail   = array();
    private $InitialReward   = array();
    private $InitialFlags    = array();

    private function __constract()
    {
        echo 'I Singreton';
    }

    /**
     * フラグ情報配列
     *
     * @var array
     */
    public $FlagTable = array(
        'Initial'           => 'a',  // 初期状態
        'StartUsing'        => 'b',  // 利用開始
        'FaceRegist'        => 'c',  // 顔登録済み
        'AddInfomation'     => 'd',  // 詳細登録済み
    );

    public static function Call(): UserHelper
    {
        if (!isset(self::$UserHelper)) {
            self::$UserHelper = new UserHelper();
        }
        return self::$UserHelper;
    }

    /**
     * 20桁の乱数を生成
     *
     * @param integer $length
     * @return string
     */
    public function getRandom(int $length = 20): string
    {
        return substr(base_convert(hash('sha256', uniqid()), 16, 36), 0, $length);
    }

    public function getInitialUserData(): array
    {
        return $this->InitialUser;
    }

    public function getInitialCardData(): array
    {
        return $this->InitialCard;
    }

    public function getInitialFaceData(): array
    {
        return $this->InitialFace;
    }

    public function getInitialDetailData(): array
    {
        return $this->InitialDetail;
    }

    public function getInitialRewardData(): array
    {
        return $this->InitialReward;
    }

    public function getInitialFlagsData(): array
    {
        return $this->InitialFlags;
    }

    public function buildInitialData(array $rsa_keys): UserHelper
    {
        $this->resetInitialData();
        for ($i=0; $i < count($rsa_keys); $i++) { 
            $this->InitialUser[$i] = $this->buildInitialUserData();
            $this->InitialCard[$i] = $this->buildInitialCardData(array(
                'card_id'       => $this->InitialUser[$i]['card_id'],
                'user_id'       => $this->InitialUser[$i]['user_id'],
                'PublicKey'     => $rsa_keys[$i]['PublicKey'],
                'PrivateKey'    => $rsa_keys[$i]['PrivateKey']
            ));
            $this->InitialFace[$i] = $this->buildInitialFaceData(array(
                'face_id'       => $this->InitialUser[$i]['face_id'],
                'user_id'       => $this->InitialUser[$i]['user_id'],
            ));
            $this->InitialDetail[$i] = $this->buildInitialUserDetailData(array(
                'detail_id'     => $this->InitialUser[$i]['detail_id'],
                'user_id'       => $this->InitialUser[$i]['user_id'],
            ));
            $this->InitialFlags[$i] = $this->buildInitialFlagData(array(
                'flag_id'       => $this->InitialUser[$i]['flag_id'],
                'user_id'       => $this->InitialUser[$i]['user_id'],
            ));
        }
        return $this;
    }

    public function buildInitialUserData(): array
    {
        return array(
            'user_id'           => $this->getRandom(),
            'detail_id'         => $this->getRandom(),
            'card_id'           => $this->getRandom(),
            'face_id'           => $this->getRandom(),
            'reward_id'         => $this->getRandom(),
            'flag_id'           => $this->getRandom(),
        );
    }

    public function buildInitialCardData(array $prop): array
    {
        return array(
            'card_id'       => $prop['card_id'],
            'user_id'       => $prop['user_id'],
            'pubkey'        => $prop['PublicKey'],
            'privkey'       => $prop['PrivateKey'],
        );
    }

    public function buildInitialFaceData(array $prop): array
    {
        return array(
            'face_id'       => $prop['face_id'],
            'user_id'       => $prop['user_id'],
            'face_facade'   => '',
            'face_left'     => '',
            'face_right'    => '',
            'face_up'       => '',
            'face_down'     => '',
        );
    }

    public function buildInitialUserDetailData(array $prop): array
    {
        return array(
            'detail_id'     => $prop['detail_id'],
            'user_id'       => $prop['user_id'],
            'type'          => 0,
            'address1'      => '',
            'address2'      => '',
            'address3'      => '',
            'phone'         => '',
            'mail'          => '',
        );
    }
    public function buildInitialFlagData(array $prop): array
    {
        return array(
            'flag_id'       => $prop['flag_id'],
            'user_id'       => $prop['user_id'],
        );
    }

    private function resetInitialData(): void
    {
        $this->InitialUser = array();
        $this->InitialCard = array();
        $this->InitialFace = array();
        $this->InitialDetail = array();
    }

    public function hydePkey(array $vals)
    {
        foreach ($vals as $key => $val) {
            $vals[$key]['privkey'] = self::getFragmentKey($val['privkey']);
        }
        return $vals;
    }

    private function getFragmentKey(string $key): string
    {
        $ch1 = substr($key, 94, 20);
        $ch1 .= substr($key, 160, 20);
        $ch1 .= substr($key, 226, 20);
        $ch1 .= substr($key, 292, 20);
        return $ch1;
    }
}

