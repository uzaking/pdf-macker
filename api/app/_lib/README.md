## セットアップ

app直下にライブラリディレクトリを配置
ディレクトリ名は「_lib」

「conf/app.php」に下記を追記
``` php
    'providers' => [
        .....
        App\_lib\MasterProvider::class,
    ],

    'aliases' => [
        ......
        'Master' => App\_lib\MasterProvider::class,
    ],
```


## 利用

・利用するコンポーネントからUse宣言をし
``` php
use App\_lib\Master;
```
・呼び出す
``` php
Master::Test1()->uza1();
```
