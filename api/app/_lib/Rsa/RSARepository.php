<?php

namespace App\_lib\RSA;

class RSARepository
{
    // RSA鍵情報配列
    private $RSAKey        = array();
    // RSA鍵情報配列、複数
    private $RSAKeys        = array();
    // 暗号化、復号化データ
    private $Result         = '';

    /**
     * アルゴリズム：推奨値はsha512だが、クライアント側がsha256までの対応となる
     * 鍵長　　　　：秘匿性の高いデータの場合は4096推奨、
     * 　　　　　　　鍵長が増えるごとに生成時の処理が10倍程度増える
     * 鍵種別　　　：「OPENSSL_KEYTYPE_RSA」一択
     * 
     */
    private $keyConfig = array(
        'dgst_alg'          => 'sha256',
        'private_key_bits'  => 2048,
        'private_key_type'  => OPENSSL_KEYTYPE_RSA,
    );

    /**
     * 複数のRSA鍵を新規作成
     *
     * @param int $count 作成数
     * @return RSARepository
     */
    public function buildNewRSAKeysMulti(int $count): RSARepository
    {
        for ($i=0; $i < $count; $i++) { 
            $this->buildNewRSAKey();
        }
        
        return $this;
    }

    /**
     * RSA鍵を新規作成
     *
     * @return RSARepository
     */
    public function buildNewRSAKey(): RSARepository
    {
        $res = openssl_pkey_new($this->keyConfig);
        openssl_pkey_export($res, $pkey);

        $pubkey_detail = openssl_pkey_get_details($res);
        $pubkey = $pubkey_detail["key"];
        
        $this->addRSAKey($pubkey, $pkey, $pubkey_detail['bits']);
        /*
        echo $pkey, PHP_EOL;
        echo $pubkey, PHP_EOL;
        echo $pubkey_detail['bits'], PHP_EOL;
        echo $pubkey_detail['type'], PHP_EOL;
        */
        return $this;
    }

    /**
     * 鍵情報の登録
     *
     * @param string $pub
     * @param string $priv
     * @param int $bits
     * @return RSARepository
     */
    public function addRSAKey(string $pub, string $priv, int $bits = 2048): RSARepository
    {
        $this->RSAKey = array(
            'PrivateKey'  => $priv,
            'PublicKey'   => $pub,
            'Bits'        => $bits,
        );
        array_push($this->RSAKeys, array(
            'PrivateKey'  => $priv,
            'PublicKey'   => $pub,
            'Bits'        => $bits,
        ));
        return $this;
    }

    /**
     * 鍵作成アルゴリズムの登録
     * 未指定の項目はデフォルトで置換
     *
     * @param array $param ['dgst_alg', 'private_key_bits', 'private_key_type']
     * @return RSARepository
     */
    public function setKeyConfig(array $param): RSARepository
    {
        $this->keyConfig['dgst_alg'] = $this->pCheck($param, 'dgst_alg');
        $this->keyConfig['private_key_bits'] = $this->pCheck($param, 'private_key_bits');
        $this->keyConfig['private_key_type'] = $this->pCheck($param, 'private_key_type');
        return $this;
    }

    /**
     * 最後に作成された鍵ペア取得
     *
     * @return array 鍵情報配列
     */
    public function getRSAKey(): array
    {
        return $this->RSAKey;
    }

    /**
     * 全ての作成された鍵ペア取得
     *
     * @return array 鍵情報配列
     */
    public function getRSAKeys(): array
    {
        return $this->RSAKeys;
    }

    /**
     * 暗号化、復号化の結果取得
     *
     * @return string
     */
    public function getResult(): string
    {
        return $this->Result;
    }

    /**
     * 秘密鍵で復号化
     *
     * @param string $val 暗号化データ(base64変換データ)
     * @return RSARepository
     */
    public function privateDecrypt(string $val): RSARepository
    {
        $this->resetResult();
        try {
            openssl_private_decrypt(
                base64_decode($val),
                $this->Result,
                $this->RSAKey['PrivateKey'],
                OPENSSL_PKCS1_PADDING
            );
        } catch (\Throwable $th) {
            $this->Result = $th;
        }

        return $this;
    }

    /**
     * 公開鍵で復号化
     *
     * @return RSARepository
     */
    public function publicDecrypt(string $val): RSARepository
    {
        $this->resetResult();
        try {
            openssl_public_decrypt(
                base64_decode($val),
                $this->Result,
                $this->RSAKey['PublicKey'],
                OPENSSL_PKCS1_PADDING
            );
        } catch (\Throwable $th) {
            $this->Result = $th;
        }

        return $this;
    }

    /**
     * 秘密鍵で暗号化
     *
     * @param string $val
     * @return string base64文字列
     */
    public function privateEncrypt(string $val): RSARepository
    {
        $this->resetResult();
        openssl_private_encrypt(
            $val,
            $crypted,
            $this->RSAKey['PrivateKey'],
            OPENSSL_PKCS1_PADDING
        );
        $this->Result = base64_encode($crypted);

        return $this;
    }

    /**
     * 公開鍵で暗号化
     *
     * @param string $val
     * @return string base64文字列
     */
    public function publicEncrypt(string $val): RSARepository
    {
        $this->resetResult();
        openssl_public_encrypt($val, $crypted, $this->RSAKey['PublicKey'], OPENSSL_PKCS1_PADDING);
        $this->Result = base64_encode($crypted);

        return $this;
    }

    public function verify($val, $sigunature)
    {
        return openssl_verify(
            $val,
            base64_decode($sigunature),
            $this->RSAKey['PublicKey'],
            OPENSSL_ALGO_SHA256
        );
    }

    /**
     * 鍵生成オプションチェック、未設定の場合はデフォルト値を使用
     *
     * @param [type] $param
     * @param [type] $key
     * @return void
     */
    private function pCheck($param, $key)
    {
        return array_key_exists($key, $param)? $param[$key] : $this->keyConfig[$key];
    }

    /**
     * 検証結果を初期化
     *
     * @return RSARepository
     */
    private function resetResult(): RSARepository
    {
        $this->Result = '';
        return $this;
    }

    /**
     * 鍵情報を初期化
     *
     * @return RSARepository
     */
    private function resetRSAKeys(): RSARepository
    {
        $this->RSAKey = array();
        return $this;
    }


}

