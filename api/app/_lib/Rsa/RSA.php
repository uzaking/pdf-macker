<?php
namespace App\_lib\RSA;
use Illuminate\Support\Facades\Facade;

class RSA extends Facade
{
    /**
     * ファサード呼び出し時にどのサービスプロバイダーを呼び出すか
     */
    protected static function getFacadeAccessor()
    {
        return 'RSA';
    }
}
