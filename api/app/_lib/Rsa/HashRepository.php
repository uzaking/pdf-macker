<?php

namespace App\_lib\RSA;

class HashRepository
{
    /**
     * md5ハッシュを返す
     *
     * @param string $val
     * @param boolean $binary
     * @return string
     */
    static public function getMD5(string $val, bool $binary = false): string
    {
        return md5($val, $binary);
    }

    /**
     * sha1ハッシュを返す
     *
     * @param string $val
     * @param boolean $binary
     * @return string
     */
    static public function getSHA1(string $val, bool $binary = false): string
    {
        return sha1($val, $binary);
    }

    private $ALGO = '';
    private $DATA = '';


    /**
     * ハッシュアルゴリズムを指定
     *
     * よく使うやつ[md5, sha1, sha256, sha512, sha512/256]
     * @param string $algo
     * @return HashRepository
     */
    public function setAlgo(string $algo): HashRepository
    {
        $algos = hash_algos();
        if (in_array($algo, $algos)) {
            $this->ALGO = $algo;
        }
        return $this;
    }

    /**
     * 計算データ設定
     *
     * @param string $data
     * @return HashRepository
     */
    public function setData(string $data): HashRepository
    {
        $this->DATA = $data;
        return $this;
    }

    /**
     * ハッシュ計算
     *
     * @param boolean $binary
     * @return string
     */
    public function getHash($binary = false): string
    {
        return hash($this->ALGO, $this->DATA, $binary);
    }
}

