<?php

namespace App\_lib\RSA;
//use App\_lib\Rsa\RSAProvider;
use Illuminate\Support\ServiceProvider;

class RSAServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->bind(
            'RSA',
            'App\_lib\RSA\RSAProvider'
        );
        /*$this->app->singleton(RsaProvider::class, function ($app) {
            return new RsaProvider();
        });*/
    }
}