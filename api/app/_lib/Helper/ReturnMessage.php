<?php

namespace App\_lib\Helper;

class ReturnMessage
{

    /**
     * Infoメッセージを返す
     *
     * @param string $message
     * @param array $data
     * @return boolean
     */
    public static function Info(
        string $message, array $data = null
    ): array {
        return array(
            'status'        => 'INFO',
            'message'       => $message
        );
    }

    public static function Success(
        string $message, array $data = null
    ): array {
        return array(
            'status'        => 'SUCCESS',
            'message'       => $message
        );
    }

    public static function Error(
        string $message, array $data = null
    ): array {
        return array(
            'status'        => 'ERROR',
            'message'       => $message
        );
    }
}
