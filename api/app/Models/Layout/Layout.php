<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    use HasFactory;
    protected $table = 'layout';
    public $timestamps = false;

    public static function add(object $layout): void
    {
        dump($layout->texts);
        $_layout = new self;
        $_layout->template         = $layout->template;
        $_layout->group1           = $layout->group1;
        $_layout->group2           = $layout->group2;
        $_layout->group3           = $layout->group3;
        $_layout->name             = $layout->name;
        $_layout->paper            = $layout->paper;
        $_layout->width            = $layout->width;
        $_layout->height           = $layout->height;
        $_layout->texts            = json_encode($layout->texts);
        $_layout->images           = json_encode($layout->images);
        $_layout->svg              = $layout->svg;
        $_layout->sheet            = $layout->sheet;
        $_layout->memo             = $layout->memo;
        $_layout->updated          = time();
        $_layout->created          = time();
        $_layout->save();

    }

    public static function change(object $layout): void
    {
        $_layout = self::find($layout->id);
        $_layout->template         = $layout->template;
        $_layout->group1           = $layout->group1;
        $_layout->group2           = $layout->group2;
        $_layout->group3           = $layout->group3;
        $_layout->name             = $layout->name;
        $_layout->paper            = $layout->paper;
        $_layout->width            = $layout->width;
        $_layout->height           = $layout->height;
        $_layout->texts            = json_encode($layout->texts);
        $_layout->images           = json_encode($layout->images);
        $_layout->svg              = $layout->svg;
        $_layout->sheet            = $layout->sheet;
        $_layout->memo             = $layout->memo;
        $_layout->updated          = time();
        $_layout->save();
    }

    public static function del(object $layout): void
    {
        dump($layout);
        self::where('id', $layout->id)->delete();
    }
    
    public static function getAll(): array
    {
        return self::get()->toArray();
    }

    public static function getById(int $id): array
    {
        return self::where('id', $id)
                    ->get()->toArray();
    }

    public static function getByTemplate(int $template): array
    {
        return self::where('template_id', $template)
                    ->get()->toArray();
    }

    public static function getByGroup1(int $group): array
    {
        return self::where('group1_id', $group)
                    ->get()->toArray();
    }

    public static function getByGroup2(int $group): array
    {
        return self::where('group2_id', $group)
                    ->get()->toArray();
    }

    public static function getByGroup3(int $group): array
    {
        return self::where('group3_id', $group)
                    ->get()->toArray();
    }
}
