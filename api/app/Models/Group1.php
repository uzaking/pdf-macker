<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group1 extends Model
{
    use HasFactory;
    protected $table = 'group1';
    public $timestamps = false;

    public static function add(object $group): void
    {
        $_group = new self;
        $_group->name             = $group->name;
        $_group->memo             = $group->memo;
        $_group->updated          = time();
        $_group->created          = time();
        $_group->save();
    }

    public static function change(object $group): void
    {
        $_group = self::find($group->id);
        $_group->name             = $group->name;
        $_group->memo             = $group->memo;
        $_group->updated          = time();
        $_group->save();
    }

    public static function del(): void
    {

    }

    public static function getById(int $id): array
    {
        return self::where('id', $id)
                    ->get()->toArray();
    }

    public static function getAll(): array
    {
        return self::get()->toArray();
    }
}
