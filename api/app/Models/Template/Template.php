<?php

namespace App\Models\Template;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;
    protected $table = 'template';
    public $timestamps = false;

    public static function add(object $template): void
    {
        $_template = new self;
        $_template->group1           = $template->group1;
        $_template->group2           = $template->group2;
        $_template->group3           = $template->group3;
        $_template->name             = $template->name;
        $_template->svg              = $template->svg;
        $_template->sheet            = $template->sheet;
        $_template->paper            = $template->paper;
        $_template->orientation      = $template->orientation;
        $_template->memo             = $template->memo;
        $_template->updated          = time();
        $_template->created          = time();
        $_template->save();
    }

    public static function change(object $template): void
    {
        $_template = self::find($template->id);
        $_template->group1           = $template->group1;
        $_template->group2           = $template->group2;
        $_template->group3           = $template->group3;
        $_template->name             = $template->name;
        $_template->svg              = $template->svg;
        $_template->sheet            = $template->sheet;
        $_template->paper            = $template->paper;
        $_template->orientation      = $template->orientation;
        $_template->memo             = $template->memo;
        $_template->updated          = time();
        $_template->save();
    }

    public static function del(): void
    {

    }

    public static function getAll(): array
    {
        return self::get()->toArray();
    }

    public static function getById(int $id): array
    {
        return self::where('id', $id)
                    ->get()->toArray();
    }
    public static function getByGroup1(int $group): array
    {
        return self::where('group1_id', $group)
                    ->get()->toArray();
    }

    public static function getByGroup2(int $group): array
    {
        return self::where('group2_id', $group)
                    ->get()->toArray();
    }

    public static function getByGroup3(int $group): array
    {
        return self::where('group3_id', $group)
                    ->get()->toArray();
    }
}
