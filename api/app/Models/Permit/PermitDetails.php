<?php

namespace App\Models\Permit;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermitDetails extends Model
{
    use HasFactory;
    protected $table = 'permit_details';
    public $timestamps = false;


    public static function addPermit(array $permit): void
    {
        $_permit = new self;
        $_permit->request_id              = $photo['request_id'];
        $_permit->permits_id              = $photo['permits_id'];
        $_photo->type                     = $photo['type'];
        $_photo->create                   = $photo['create'];
        $_photo->save();
    }

    public static function getPermitById(int $id): array
    {
        return self::where('id', $id)->first();
    }

    public static function getPermitByRequestId(string $requestId): array
    {
        return self::where('request_id', $requestId)->array();
    }

}
