<?php

namespace App\Models\Image;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    protected $table = 'images';
    public $timestamps = false;

    public static function add(object $image): void
    {
        $_image = new self;
        $_image->name             = $image->name;
        $_image->image            = $image->image;
        $_image->memo             = $image->memo;
        $_image->updated          = time();
        $_image->created          = time();
        $_image->save();
    }

    public static function change(object $image): void
    {
        $_image = self::find($image->id);
        $_image->name             = $image->name;
        $_image->image            = $image->image;
        $_image->memo             = $image->memo;
        $_image->updated          = time();
        $_image->save();
    }

    public static function del(): void
    {

    }
    public static function getAll(): array
    {
        return self::get()->toArray();
    }
    public static function getById(int $id): array
    {
        return self::where('id', $id)
                    ->get()->toArray();
    }
}
