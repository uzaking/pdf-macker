
CREATE TABLE `template` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `group1` int(10) NOT NULL DEFAULT 0,
  `group2` int(10) NOT NULL DEFAULT 0,
  `group3` int(10) NOT NULL DEFAULT 0,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `svg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sheet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `paper` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orientation` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE `layout` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `template_id` int(10) NOT NULL,
  `group1` int(10) NOT NULL DEFAULT 0,
  `group2` int(10) NOT NULL DEFAULT 0,
  `group3` int(10) NOT NULL DEFAULT 0,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texts` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `svg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sheet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `images` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `group1` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `group2` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `group3` (
  `id` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated` int(11) NOT NULL DEFAULT 1111111111,
  `created` int(11) NOT NULL DEFAULT 1111111111
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

